import os
from configparser import ConfigParser
from urllib.error import HTTPError

import requests
from passlib.hash import argon2
from psycopg2 import connect
import uuid

CONFIG_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini')
CONFIG_SECTION = 'database'

conn = None
cur = None



def category_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "id": data[0],
            "category_name": data[1]
        }
        array.append(dict)
    return array


def product_serializer(data_list):
    id=set()
    array =[]
    list={}
    for data in data_list:
        id.add(data[0])
    for i in id:
        amount = {}
        for data in data_list:
            if data[0]==i:
                amount[data[5]]=data[6]
        list[i]=amount
    for data in data_list:
        for i in id:
            if data[0]==i:
                dict = {
                    "id": data[0],
                    "product_name": data[1],
                    "product_price": data[2],
                    "img_url": data[3],
                    "desc": data[4],
                    "amount":list[data[0]],
                    "category": {
                        "category_id": data[7],
                        "category_name": data[8],
                        "is_active": data[9]
                    }
                }
                if dict not in array:
                    array.append(dict)

    return array

def connect_to_db():
    global conn
    global cur

    conf_pars = ConfigParser()
    conf_pars.read(CONFIG_FILE)

    conn = connect(
        f"host={conf_pars[CONFIG_SECTION]['host']} dbname={conf_pars[CONFIG_SECTION]['database']} user={conf_pars[CONFIG_SECTION]['user']} password={conf_pars[CONFIG_SECTION]['password']}")
        #TODO implement no hardcoded arguments
        #"host='localhost' dbname='online_shop_preloaded' user='postgres' password='admin'")

    conn.autocommit = False
    cur = conn.cursor()

def validate_login(email, password):
    connect_to_db()

    sql = f"""
        SELECT "Users"."UserId", "Email", "FirstName", "LastName", "Address", "PasswordHash", "EmailConfirmed", "RoleName","Users"."IsActive"
        FROM "Users" LEFT JOIN "UserRoles" 
        ON "Users"."UserId" = "UserRoles"."UserId"
        LEFT JOIN "Roles"
        ON "UserRoles"."RoleId" = "Roles"."RoleId"
        WHERE "Email" = '{email}'"""


    try:
        cur.execute(sql)
        user_id, email, first_name, last_name, address, pwd_hash, email_confirmed, role,active = cur.fetchone()
    except TypeError:
        return None, 'No such user'
    except Exception as e:
        print(e)
        return None, 'Database error'
    if active == False:
        return None, 'User is inactive'

    if argon2.verify(password, pwd_hash):
        return (user_id, email, first_name, last_name, email_confirmed, role), None
    else:
        return None, 'Wrong authentification data'


def post_user(req):
    connect_to_db()

    sql = f"""
        SELECT * FROM "Users"
        WHERE "Email" = '{req["email"]}'
    """

    try:
        cur.execute(sql)
        data = cur.fetchall()
        if len(data) != 0:
            raise Exception("User already in database")
        id = uuid.uuid4()
        sql_insert_user = f"""
        INSERT INTO "Users" ("UserId", "Email", "FirstName", "LastName", "Address", "PasswordHash", "EmailConfirmed")
        VALUES ('{id}', '{req["email"]}', '{req["firstName"]}', '{req["lastName"]}', '{req["address"]}', '{argon2.hash(req["password"])}', '{True}')
        """

        sql_insert_user_role = f"""
        INSERT INTO "UserRoles" ("UserId", "RoleId")
        VALUES ('{id}', 3)
        """

        try:
            cur.execute(sql_insert_user)
            cur.execute(sql_insert_user_role)
            conn.commit()
            return ('Successfully added user', id), None
        except:
            return None, 'User not added'
    except Exception as e:
        if 'User already in database' in str(e):
            return None, 'User already in database'
        return None, 'Database error'


def activate_account(id):
    connect_to_db()

    sql = f"""
    UPDATE "Users" SET "EmailConfirmed" = TRUE 
    WHERE "UserId" = '{id}'
    """

    try:
        cur.execute(sql)
        conn.commit()
        return 'User successfully activated', None
    except Exception as e:
        print(e)
        return None, 'Database error'



def edit_profile(req, userid):
    connect_to_db()

    sql = f"""
        UPDATE "Users"
        SET "Email"='{req["email"]}', "FirstName"='{req["first_name"]}',"LastName"='{req["last_name"]}'
        WHERE "UserId"='{userid}'
    """
    sql_get=f"""
        SELECT "Users"."EmailConfirmed","Roles"."RoleName"
        FROM "Users" NATURAL JOIN "UserRoles" NATURAL JOIN "Roles"
        WHERE "Users"."UserId"='{userid}'
    """
    try:
        cur.execute(sql)
        conn.commit()
        cur.execute(sql_get)
        data = cur.fetchall()
        return (userid,req["email"], req["first_name"], req["last_name"],data[0][0],data[0][1]), None
    except Exception as e:
        print(e)
        return None, 'Database error'


def get_products(id):
    connect_to_db()
    sql = """
        SELECT "Products"."ProductId", "ProductName", "ProductPrice","ImgURL", "Description","SizeName","Stock"."Amount", "Categories"."CategoryId", "Categories"."CategoryName", "Categories"."IsActive"
        FROM "Products" LEFT JOIN "Sizes" ON "Products"."SizeId"="Sizes"."SizeId"
        LEFT JOIN "Stock" ON "Products"."SizeId"="Stock"."StockSizeId" AND "Products"."ProductId"="Stock"."StockProductId"
        LEFT JOIN "ProductCategories" ON "Products"."ProductId" = "ProductCategories"."ProductId"
        LEFT JOIN "Categories" ON "ProductCategories"."CategoryId" = "Categories"."CategoryId"
        WHERE "Products"."IsActive" = 'True'
        """
    if id is not None:
        sql += f"""AND "Products"."ProductId" = {id} 
                """

    try:
        cur.execute(sql)
        data = cur.fetchall()
        #print(data)
    except Exception as e:
        print(e)
        return None, 'Database error'
    dict_data = product_serializer(data)
    print(dict_data)
    return dict_data, None

def post_product(req,file):
    #print(req)
    connect_to_db()
    count_products = """
    SELECT COUNT(*)+1 FROM "Products"
    """

    cur.execute(count_products)
    id_product = cur.fetchone()[0]
    sql_get_sizes="""
        SELECT *
        FROM "Sizes"
    """
    cur.execute(sql_get_sizes)
    sizes = cur.fetchall()
    amount = {1: req["amount"]["S"], 2: req["amount"]["M"], 3: req["amount"]["L"]}
    for s in sizes:
        sql_insert = f"""
            INSERT INTO "Products" ("ProductId", "ProductName", "ProductPrice", "ImgURL", "Description", "SizeId","IsActive")
            VALUES ({id_product},
                    '{req["product_name"]}',
                    '{req["product_price"]}',
                    '',
                    '{req["desc"]}',
                    '{s[0]}',
                    'True')
            """
        sql_insert2=f"""
            INSERT INTO "Stock" ("StockProductId", "StockSizeId", "Amount","IsActive")
            VALUES ('{id_product}','{s[0]}','{amount[s[0]]}','True')
            """
        try:

            cur.execute(sql_insert)
            conn.commit()
            cur.execute(sql_insert2)
            conn.commit()
        except Exception as e:
            print(e)
            return None, e

    sql_category=f"""
        SELECT "CategoryId"
        FROM "Categories"
        WHERE "CategoryName" LIKE '{req['category_name']}'
    """
    cur.execute(sql_category)
    id_category = cur.fetchone()[0]


    count_products = """
        SELECT COUNT(*)+1 FROM "ProductCategories"
        """
    cur.execute(count_products)
    id_product_categories = cur.fetchone()[0]

    sql_insert_2 = f"""
        INSERT INTO "ProductCategories" ("ProductCategoryId", "ProductId", "CategoryId")
        VALUES ({id_product_categories}, {id_product}, {id_category})
    """

    try:
        cur.execute(sql_insert_2)
        conn.commit()
    except Exception as e:
        print(e)
        return None, 'Database error'
    try:
        if len(file.getlist('file')) != 0:

            import boto3, botocore
            from app import current_app
            print(current_app.config['S3_SECRET_ACCESS_KEY'])
            print(current_app.config['S3_KEY'])
            s3 = boto3.client(
                "s3",
                aws_access_key_id=current_app.config['S3_KEY'],
                aws_secret_access_key=current_app.config['S3_SECRET_ACCESS_KEY']
            )

            upload = upload_file_to_s3(file['file'], bucket=s3)

            sql_update = f"""
                           UPDATE "Products"
                           SET "ImgURL" = '{upload}'
                           WHERE "ProductId" = {id_product}
                           """

            try:
                cur.execute(sql_update)
                conn.commit()
            except Exception as e:
                print(e)
                return None, 'Database error'

        return 'Successfully added product', None
    except Exception as e:
        print(e)
        return None, 'Database error'




def put_product(req, product_id, file=None):
    print(req,product_id,file)
    connect_to_db()
    sql=f"""
        UPDATE "Products"
        SET "ProductName"='{req["product_name"]}',
            "ProductPrice"={req["product_price"]},
            "Description"='{req["desc"]}'
        WHERE "ProductId" = {product_id}
        """
    sql_get=f"""
        SELECT "StockSizeId"
        FROM "Stock"
        WHERE "StockProductId"='{product_id}'
    """

    try:
        cur.execute(sql)
        conn.commit()

        cur.execute(sql_get)
        data=cur.fetchall()
        sizes=[1,2,3]
        amount={1:req["amount"]["S"],2:req["amount"]["M"],3:req["amount"]["L"]}
        for s in sizes:
            true=0
            r = {
                "stock_product_id": product_id,
                "stock_size_id": s,
                "amount": amount[s]
            }
            for d in data:
                if d[0] == s:
                    true=1
            if true==0:
                post_stock(r)
            else:
                put_stock(r)

        if len(file.getlist('file')) != 0:
            from werkzeug.utils import secure_filename
            import boto3, botocore
            from app import current_app
            print(current_app.config['S3_SECRET_ACCESS_KEY'])
            print(current_app.config['S3_KEY'])
            s3 = boto3.client(
                "s3",
                aws_access_key_id=current_app.config['S3_KEY'],
                aws_secret_access_key=current_app.config['S3_SECRET_ACCESS_KEY']
            )

            upload = upload_file_to_s3(file['file'], bucket=s3)

            sql_update = f"""
                    UPDATE "Products"
                    SET "ImgURL" = '{upload}'
                    WHERE "ProductId" = {product_id}
                    """

            try:
                cur.execute(sql_update)
                conn.commit()
            except Exception as e:
                print(e)
                return None, 'Database error'

        return 'Successfully updated product',None
    except Exception as e:
        print(e)
        return None, 'Database error'


def delete_product(id):
    connect_to_db()
    sql_update = f"""
        UPDATE "Products"
        SET "IsActive" = 'False'
        WHERE "ProductId" = {id}
        """

    try:
        cur.execute(sql_update)
        conn.commit()
        return 'Successfully deleted product' ,None
    except Exception as e:
        print(e)
        return None, 'Database error'



def post_category(req):
    connect_to_db()
    sql_insert = f"""
        INSERT INTO "Categories" ("CategoryName", "IsActive")
        VALUES ('{req["category_name"]}','True')
        """

    try:
        cur.execute(sql_insert)
        conn.commit()
        return 'Successfully added category',None
    except Exception as e:
        print(e)
        return None, 'Database error'


def get_categories(id):
    connect_to_db()
    sql = """
    SELECT *
    FROM "Categories"
    WHERE "IsActive" = 'True'
    """

    if id is not None:
        sql = f"""
                SELECT "Products"."ProductId", "ProductName", "ProductPrice","ImgURL", "Description","SizeName","Stock"."Amount", "Categories"."CategoryId", "Categories"."CategoryName", "Categories"."IsActive"
                FROM "Products" LEFT JOIN "Sizes" ON "Products"."SizeId"="Sizes"."SizeId"
                LEFT JOIN "Stock" ON "Products"."SizeId"="Stock"."StockSizeId" AND "Products"."ProductId"="Stock"."StockProductId"
                LEFT JOIN "ProductCategories" ON "Products"."ProductId" = "ProductCategories"."ProductId"
                LEFT JOIN "Categories" ON "ProductCategories"."CategoryId" = "Categories"."CategoryId"
                WHERE "Products"."IsActive" = 'True' AND "Categories"."CategoryId" = {id}
                """

        try:
            cur.execute(sql)
            data = cur.fetchall()
        except Exception as e:
            print(e)
            return None, 'Database error'

        dict_data = product_serializer(data)

        return dict_data, None

    try:
        cur.execute(sql)
        data = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'

    dict_data = category_serializer(data)

    return dict_data, None

def delete_category(id):
    print(id)
    connect_to_db()
    sql = f"""
        SELECT *
        FROM "ProductCategories"
        WHERE "CategoryId" = {id}
        """
    sql_update = f"""
            UPDATE "Categories"
            SET "IsActive" = 'False'
            WHERE "CategoryId" = {id}
            """


    try:
        cur.execute(sql)
        data = cur.fetchall()
        for d in data:
            delete_product(d[1])
    except Exception as e:
        print(e)
        return None, 'Database error'

    try:
        cur.execute(sql_update)
        conn.commit()
        return 'Successfully deleted category',None
    except Exception as e:
        print(e)
        return None, 'Database error'



def bills_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "bill_id": data[0],
            "pdv": data[1],
            "zki": data[2],
            "jir": data[3],
            "time_date": data[4],
            "price": data[5]
        }
        array.append(dict)
    return array


def get_bills(id):
    connect_to_db()
    sql="""
    SELECT *
    FROM "Bills"
    WHERE "IsActive" ='True'
    """

    if id is not None:
        sql += f"""AND "BillId" = {id} """

    try:
        cur.execute(sql)
        data = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    dict_data = bills_serializer(data)

    return dict_data, None


def post_bills(req):
    connect_to_db()
    sql_insert = f"""
         INSERT INTO "Bills" ("PDV", "ZKI", "JIR", "TimeDate","Price","IsActive")
         VALUES ('{req["pdv"]}',
                 '{req["zki"]}',
                 '{req["jir"]}',
                 '{req["time_date"]}',
                 '{req["price"]}',
                 'True')
         """
    try:
        cur.execute(sql_insert)
        conn.commit()
        return 'Successfully added bill',None
    except Exception as e:
        print(e)
        return None, 'Database error'


def delete_bills(id):
    connect_to_db()
    sql_update = f"""
                UPDATE "Bills"
                SET "IsActive" = 'False'
                WHERE "BillId" = '{id}'
                """
    try:
        cur.execute(sql_update)
        conn.commit()
        return 'Successfully deleted bill',None
    except Exception as e:
        print(e)
        return None, 'Database error'




def basket_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "basket_id": data[0],
            "product_id": data[1],
            "img_url":data[2],
            "product_price":data[3],
            "size_id": data[4],
            "amount": data[5],
            "user_basket_id": data[6],
            "product_name" :data[7],
            "size_name" : data[8]
        }
        array.append(dict)
    return array


def get_baskets(id,active):
    connect_to_db()
    if active==1:
        active= True
    else:
        active=False
    sql=f"""
    SELECT "BasketId","Products"."ProductId","ImgURL","ProductPrice","Products"."SizeId","Amount","Baskets"."UserBasketId","ProductName","SizeName"
    FROM "Baskets" JOIN "UserBaskets" ON "Baskets"."UserBasketId" = "UserBaskets"."UserBasketId"
    LEFT JOIN "Products" ON "Products"."SizeId" = "Baskets"."SizeId" AND "Products"."ProductId" = "Baskets"."ProductId"
    LEFT JOIN "Sizes" ON "Products"."SizeId" = "Sizes"."SizeId"
    WHERE "UserId" = '{id}' AND "Baskets"."IsActive" ='True'
            AND "UserBaskets"."IsActive" ='{active}'
    """

    try:
        cur.execute(sql)
        data = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    dict_data = basket_serializer(data)

    return dict_data, None


def post_baskets(req,id):
    connect_to_db()
    sql_get=f"""
        SELECT *
        FROM "UserBaskets"
        WHERE "UserId" ='{id}' AND "IsActive"='True'
        """

    sql_insert=f"""
        INSERT INTO "UserBaskets" ("UserId","IsActive")
        VALUES ('{id}',
                'True')
        """

    try:
        cur.execute(sql_get)
        dataId = cur.fetchall()

        if dataId == []:
            print(dataId)
            cur.execute(sql_insert)
            conn.commit()
            cur.execute(sql_get)

            dataId=cur.fetchall()
        sql_find = f"""
            SELECT *
            FROM "Baskets"
            WHERE "ProductId"='{req["product_id"]}' AND "SizeId"='{req["size_id"]}' AND "UserBasketId"='{dataId[0][0]}'
            """
        cur.execute(sql_find)
        data = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    if data==[]:
        sql=f"""
            INSERT INTO "Baskets" ("ProductId", "SizeId","Amount","UserBasketId","IsActive")
            VALUES ('{req["product_id"]}',
                     '{req["size_id"]}',
                     '{req["amount"]}',
                     '{dataId[0][0]}',
                     'True')
            """
    else:

        sql = f"""
            UPDATE "Baskets" SET "Amount" ="Amount"+ '{req["amount"]}'
            WHERE "ProductId"='{req["product_id"]}' AND "SizeId"='{req["size_id"]}' AND "UserBasketId"='{dataId[0][0]}'
            """
    try:
        cur.execute(sql)
        conn.commit()
        return 'Successfully added basket',None
    except Exception as e:
        print(e)
        return None, 'Database error'


def put_baskets(req, id):
    connect_to_db()
    sql_get=f"""
        SELECT *
        FROM "UserBaskets"
        WHERE "UserId" ='{id}' AND "IsActive"='True'
        """

    try:
        cur.execute(sql_get)
        dataId = cur.fetchall()
        sql_get2=f"""
            SELECT *
            FROM "Baskets"
            WHERE "ProductId"='{req["product_id"]}' AND "SizeId"='{req["size_id"]}'
            AND "UserBasketId"='{dataId[0][0]}'
        """
        cur.execute(sql_get2)
        data2=cur.fetchall()
        print(data2)
    except Exception as e:
        print(e)
        return None, 'Database error'
    if req["amount"]==0:
        delete_baskets(data2[0][0])
        return 'Successfully deleted basket',None
    else:
        sql = f"""
            UPDATE "Baskets" SET "Amount" ='{req["amount"]}' 
            WHERE "ProductId"='{req["product_id"]}' AND "SizeId"='{req["size_id"]}' AND "UserBasketId"='{dataId[0][0]}'
            """
        try:
            cur.execute(sql)
            conn.commit()
            return 'Successfully added basket',None
        except Exception as e:
            print(e)
            return None, 'Database error'


def delete_baskets(id):
    connect_to_db()
    sql_update = f"""
                DELETE FROM "Baskets"
                WHERE "BasketId"={id}
                """
    try:
        cur.execute(sql_update)
        conn.commit()
        return 'Successfully deleted basket',None
    except Exception as e:
        print(e)
        return None, 'Database error'




def userB_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "user_basket_id": data[0],
            "sum": data[1]
        }
        array.append(dict)
    return array


def get_userbaskets(userid, id):
    connect_to_db()

    if id==None:
        sql="""SELECT "Baskets"."UserBasketId", SUM("Products"."ProductPrice" * "Amount")"""
    else:
        sql="""SELECT "BasketId","Products"."ProductId","ImgURL",
        "ProductPrice","Products"."SizeId","Amount","Baskets"."UserBasketId","ProductName","SizeName"
        """

    sql+=f"""
    FROM "Baskets" JOIN "UserBaskets" ON "Baskets"."UserBasketId" = "UserBaskets"."UserBasketId"
    LEFT JOIN "Products" ON "Products"."SizeId" = "Baskets"."SizeId" AND "Products"."ProductId" = "Baskets"."ProductId"
    LEFT JOIN "Sizes" ON "Products"."SizeId" = "Sizes"."SizeId"
    WHERE "UserId" = '{userid}'
	AND "Baskets"."IsActive" ='True'
    AND "UserBaskets"."IsActive" ='False'
    """
    if id==None:
        sql+="""GROUP BY "Baskets"."UserBasketId"
        """
    else:
        sql+=f"""AND "UserBaskets"."UserBasketId"='{id}'"""
    try:
        cur.execute(sql)
        data = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    if id==None:
        dict_data=userB_serializer(data)
    else:
        dict_data = basket_serializer(data)

    return dict_data, None


def delete_userbaskets(id):
    connect_to_db()
    sql_update = f"""
                UPDATE "Baskets"
                SET "IsActive" = 'False'
                WHERE "UserBasketId" = {id}
                """
    try:
        cur.execute(sql_update)
        conn.commit()
        return 'Successfully deleted user basket',None
    except Exception as e:
        print(e)
        return None, 'Database error'


def coupon_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "coupon_id": data[0],
            "percentage": data[1],
            "expires": data[2],
            "amount": data[3],
            "code": data[4],
            "is_active" : data[5]
        }
        array.append(dict)
    return array


def get_coupons(code):
    connect_to_db()
    sql = """
        SELECT *
        FROM "Coupons"
        """

    if code is not None:
        sql += f"""WHERE "Code" = '{code}' """

    try:
        cur.execute(sql)
        data = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    dict_data = coupon_serializer(data)

    return dict_data, None


def post_coupon(req):
    connect_to_db()
    sql_insert = f"""
            INSERT INTO "Coupons" ("Percentage", "Expires", "Amount", "Code", "IsActive")
            VALUES ('{req["percentage"]}',
                    '{req["expires"]}',
                    '{req["amount"]}',
                    '{req["code"]}',
                    'True')
            """

    try:
        cur.execute(sql_insert)
        conn.commit()
        return 'Successfully added coupon', None
    except Exception as e:
        print(e)
        return None, 'Database error'

def delete_coupon(id):
    connect_to_db()
    sql_delete = f"""
                   DELETE FROM "Coupons"
                   WHERE "CouponId" = {id}
                   """
    try:
        cur.execute(sql_delete)
        conn.commit()
        return 'Successfully deleted coupon', None
    except Exception as e:
        print(e)
        return None, 'Database error'


def stock_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "stock_id": data[0],
            "stock_product_id": data[1],
            "StockSizeId": data[2],
            "Amount": data[3]
        }
        array.append(dict)
    return array


def get_stock(id):
    connect_to_db()
    sql = """
        SELECT *
        FROM "Stock"
        WHERE "IsActive" = 'True'
        """

    if id is not None:
        sql += f"""WHERE "StockId" = {id} """

    try:
        cur.execute(sql)
        data = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    dict_data = stock_serializer(data)

    return dict_data, None


def post_stock(req):
    connect_to_db()
    sql_insert = f"""
            INSERT INTO "Stock" ("StockProductId", "StockSizeId", "Amount","IsActive")
            VALUES ('{req["stock_product_id"]}',
                    '{req["stock_size_id"]}',
                    '{req["amount"]}',
                    'True')
            """

    try:
        cur.execute(sql_insert)
        conn.commit()
        return 'Successfully added to stock', None
    except Exception as e:
        print(e)
        return None, 'Database error'

def put_stock(req):
    connect_to_db()
    sql_insert = f"""
            UPDATE "Stock" 
            SET "Amount"='{req["amount"]}',
                "IsActive"='True'
            WHERE "StockProductId"={req["stock_product_id"]} AND "StockSizeId"={req["stock_size_id"]}
            """

    try:
        cur.execute(sql_insert)
        conn.commit()
        return 'Successfully updated stock', None
    except Exception as e:
        print(e)
        return None, 'Database error'


def delete_stock(id):
    connect_to_db()
    sql=f"""
    SELECT "Amount"
    FROM "Stock"
    WHERE "StockId" = {id}
    """
    try:
        cur.execute(sql)
        amount = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    if amount == 0:
        sql_update = f"""
                UPDATE "Stock"
                SET "IsActive"= 'False'
                WHERE "StockId" = {id}
                """
    else:
        sql_update = f"""
                UPDATE "Stock"
                SET "Amount"="Amount"-1
                WHERE "StockId" = {id}
                """

    try:
        cur.execute(sql_update)
        conn.commit()
        return 'Successfully deleted from stock', None
    except Exception as e:
        print(e)
        return None, 'Database error'

def purchases_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "num_bills": data[0],
            "sum_price": str(data[1])
        }
        array.append(dict)
    return array


def get_purchases():
    connect_to_db()
    sql = """
         SELECT count("BillId"),sum("Price")
         FROM "Bills"
         WHERE "Bills"."IsActive" = 'True'
         """
    sql2 = """
         SELECT *
         FROM "Bills"
         WHERE "Bills"."IsActive" = 'True'
         """


    try:
        cur.execute(sql)
        data = cur.fetchall()
        cur.execute(sql2)
        data2 = cur.fetchall()
    except Exception as e:
        print(e)
        return None, 'Database error'
    print(data2)
    dict_data = purchases_serializer(data)

    return dict_data, None


def users_serializer(data_list):
    array = []
    for data in data_list:
        dict = {
            "user_id": data[0],
            "first_name": data[1],
            "last_name" : data[2],
            "user_role_id" : data[3],
            "role_name" : data[4]
        }
        array.append(dict)
    return array

def get_userId_by_data(req):
    connect_to_db()
    sql=f"""
        SELECT "UserId"
        FROM "Users"
        WHERE "Email"='{req["email"]}'
        AND "FirstName"='{req["firstName"]}'
        AND "LastName"='{req["lastName"]}'
        AND "Address"='{req["address"]}'
    """
    try:
        cur.execute(sql)
        userId= cur.fetchone()[0]
        print(userId)

    except Exception as e:
        print(e)
        return None, 'Database error'

    return userId, None


def get_users():
    connect_to_db()
    sql = """
         SELECT "Users"."UserId","FirstName", "LastName","UserRoleId","RoleName"
         FROM "Users" NATURAL JOIN "UserRoles" NATURAL JOIN "Roles"
         """
    try:
        cur.execute(sql)
        data = cur.fetchall()

    except Exception as e:
        print(e)
        return None, 'Database error'

    dict_data = users_serializer(data)

    return dict_data, None


def put_users(req):
    connect_to_db()
    sql = f"""
        UPDATE "UserRoles" 
        SET "RoleId" = '2'
        WHERE "UserId"='{req["user_id"]}'
        """
    try:
        cur.execute(sql)
        conn.commit()
        return 'Successfully added owner',None
    except Exception as e:
        print(e)
        return None, 'Database error'


def perm_delete_user(id):
    connect_to_db()
    sql=f"""
        DELETE FROM "Users"
        WHERE "UserId"='{id}'
    """
    try:
        cur.execute(sql)
        conn.commit()
        return 'Successfully deleted user', None
    except Exception as e:
        print(e)
        return None, 'Database error'

def delete_user(id):
    connect_to_db()

    sql1=f"""
    DELETE FROM "UserRoles"
    WHERE "UserId"='{id}'
    """
    sql2=f"""
    UPDATE "UserBaskets"
    SET "IsActive"='False'
    WHERE "UserId"='{id}'
    """
    sql3=f"""
    UPDATE "Users"
    SET "IsActive"='False'
    WHERE "UserId"='{id}'
    """
    try:
        cur.execute(sql1)
        cur.execute(sql2)
        cur.execute(sql3)
        conn.commit()
        return 'Successfully deleted user', None
    except Exception as e:
        print(e)
        return None, 'Database error'


def search_products(args):
    connect_to_db()
    return_data = []

    for arg in args:
        sql1 = f"""
            SELECT "Products"."ProductId", "ProductName", "ProductPrice","ImgURL", "Description","SizeName","Stock"."Amount", "Categories"."CategoryId", "Categories"."CategoryName", "Categories"."IsActive"
                    FROM "Products" LEFT JOIN "Sizes" ON "Products"."SizeId"="Sizes"."SizeId"
                    LEFT JOIN "Stock" ON "Products"."SizeId"="Stock"."StockSizeId" AND "Products"."ProductId"="Stock"."StockProductId"
                    LEFT JOIN "ProductCategories" ON "Products"."ProductId" = "ProductCategories"."ProductId"
                    LEFT JOIN "Categories" ON "ProductCategories"."CategoryId" = "Categories"."CategoryId"
                    WHERE "Products"."IsActive" = 'True' AND LOWER("ProductName") LIKE '%{arg}%' OR "Description" LIKE '%{arg}%'
        """

        try:
            cur.execute(sql1)
            data = cur.fetchall()
        except Exception as e:
            print(e)
            return None, 'Database error'

        dict_data = product_serializer(data)
        return_data.append(dict_data)
        print(return_data)

    return return_data, None


def checkout(req, user_id):
    connect_to_db()
    print(req)

    sql_get_user_basket = f"""
        SELECT "UserBasketId" FROM "UserBaskets"
        WHERE "UserId" = '{user_id}' AND "IsActive" = 'True'
    """


    try:
        cur.execute(sql_get_user_basket)
        user_basket_id = cur.fetchone()[0]
    except Exception as e:
        print(e)
        return None, e


    sql_get_basket = f"""
        SELECT "ProductId" FROM "Baskets"
        WHERE "UserBasketId" = {user_basket_id}
        AND "IsActive" = 'True'
    """

    try:
        cur.execute(sql_get_basket)
        product_ids = cur.fetchall()
        print(product_ids)
    except Exception as e:
        print(e)
        return None, e

    sql_make_basket_unactive = f"""
            UPDATE "UserBaskets"
            SET "IsActive" = 'False'
            WHERE "UserBasketId" = {user_basket_id}
        """

    try:
        cur.execute(sql_make_basket_unactive)
        conn.commit()
    except Exception as e:
        print(e)
        return None, e

    total_price = 0
    for product_id in product_ids:
        sql_get_product_price = f"""
            SELECT "ProductPrice" FROM "Products"
            WHERE "ProductId" = {product_id[0]}
        """

        try:
            cur.execute(sql_get_product_price)
            product_price = cur.fetchone()[0]
            total_price += product_price
        except Exception as e:
            print(e)
            return None, e

    if len(req['coupon']) == 0:
        req['coupon'] = None

    if req['coupon'] is not None:
        print("USAO")
        sql_get_coupon = f"""
            SELECT "Percentage"
            FROM "Coupons"
            WHERE "Code" = '{req['coupon']}'
        """

        sql_get_coupon_amount = f"""
            SELECT "Amount" 
            FROM "Coupons"
            WHERE "Code" = '{req['coupon']}'
        """

        try:
            cur.execute(sql_get_coupon_amount)
            amount = cur.fetchone()[0]
        except Exception as e:
            print(e)
            return None, e

        sql_remove_amount = f"""
            UPDATE "Coupons"
            SET "Amount" = {amount} - 1
            WHERE "Code" = '{req['coupon']}'
        """

        try:
            cur.execute(sql_remove_amount)
            conn.commit()
        except Exception as e:
            print(e)
            return None, e

        try:
            cur.execute(sql_get_coupon)
            percentage = cur.fetchone()[0]
            total_price = total_price * (1 - (percentage/100))
        except Exception as e:
            print(e)
            return None, e

    try:
        jir = requests.get("https://holding-fina.herokuapp.com/jir")
        array = jir.json()
        zki = requests.get("https://holding-fina.herokuapp.com/zki")
        array.update(zki.json())
        time = jir.headers['Date']
        array["time"] = time
    except HTTPError as err:
        print(f'HTTP error occurred: {err}')
        print(array)
    except Exception as err:
        print(f'Other error occurred: {err}')

    sql_insert_bill = f"""
        INSERT INTO "Bills" ("PDV", "ZKI", "JIR", "TimeDate","Price","IsActive") VALUES
        (25, '{zki}', '{jir}', '{time}', {total_price}, True)
        RETURNING *
    """

    try:
        cur.execute(sql_insert_bill)
        bill_id = cur.fetchone()[0]
        conn.commit()
    except Exception as e:
        print(e)
        return None, e

    sql_insert_purchases = f"""
        INSERT INTO "Purchases" ("UserBasketId", "BillId") VALUES
        ({user_basket_id}, {bill_id})
    """

    try:
        cur.execute(sql_insert_purchases)
        conn.commit()
    except Exception as e:
        print(e)
        return None, e

    return (product_ids, total_price, zki, jir, time), None

def get_productid_by_all_data(req):

    connect_to_db()
    sql=f"""
        SELECT "ProductId"
        FROM "Products"
        WHERE "ProductName"='{req["product_name"]}'
            AND "ProductPrice"='{req["product_price"]}'
            AND "Description"='{req["desc"]}'
            AND "IsActive"='True'
    """
    try:
        cur.execute(sql)
        data=cur.fetchone()[0]
    except Exception as e:
        print(e)
        return None, e
    print(data)
    return data,None



def get_products_by_data(product_ids):
    data = []
    for product_id in product_ids:
        sql = f"""
            SELECT "ProductName", "ProductPrice"
            FROM "Products"
            WHERE "ProductId" = {product_id[0]}
        """

        try:
            cur.execute(sql)
            data.append(cur.fetchone())
        except Exception as e:
            print(e)
            return None, e

    return data, None


import os
from werkzeug.utils import secure_filename

def upload_file_to_s3(file, acl="public-read", bucket=None):
    filename = secure_filename(file.filename)
    try:
        from app import current_app
        bucket.upload_fileobj(
            file,
            current_app.config['S3_BUCKET'],
            file.filename,
            ExtraArgs={
                "ACL": acl,
                "ContentType": file.content_type
            }
        )

    except Exception as e:
        # This is a catch all exception, edit this part to fit your needs.
        print("Something Happened: ", e)
        return e

    # after upload file to s3 bucket, return filename of the uploaded file
    return "{}{}".format(current_app.config["S3_LOCATION"], file.filename)




