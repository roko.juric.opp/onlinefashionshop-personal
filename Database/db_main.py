import os
from sqlalchemy import create_engine, MetaData
from configparser import ConfigParser
from sqlalchemy.ext.automap import automap_base
from Database.Models import DB


CONFIG_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini')
CONFIG_SECTION = 'database'

def read_config(CONFIG_FILE):
    parser = ConfigParser()
    parser.read(CONFIG_FILE)
    return parser


def create_new_database(CONFIG_FILE, CONFIG_SECTION):
    conf = read_config(CONFIG_FILE)
    DB_CONFIG_DICT = dict(conf.items(CONFIG_SECTION))

    DB_CONFIG_DICT_DEFAULT = DB_CONFIG_DICT.copy()

    DB_CONN_FORMAT = "postgresql+psycopg2://{user}:{password}@{host}/{database}"

    DB_CONN_URI_DEFAULT = (DB_CONN_FORMAT.format(**DB_CONFIG_DICT_DEFAULT))

    engine_default = create_engine(DB_CONN_URI_DEFAULT)

    conn = engine_default.connect()


    result = conn.execute("SELECT datname FROM pg_database")
    database_list = [i[0] for i in result.fetchall()]

    if DB_CONFIG_DICT["database"].lower() not in database_list:
        conn.execute("COMMIT")
        conn.execute("CREATE DATABASE {} ".format(DB_CONFIG_DICT["database"]))

    conn.close()


def create_tables(CONFIG_FILE, CONFIG_SECTION):
    conf = read_config(CONFIG_FILE)
    DB_CONFIG_DICT = dict(conf.items(CONFIG_SECTION))

    DB_CONN_FORMAT = "postgresql://{user}:{password}@{host}/{database}"
    DB_CONN_URI = DB_CONN_FORMAT.format(**DB_CONFIG_DICT)
    engine = create_engine(DB_CONN_URI)

    meta = MetaData(bind=engine)
    meta.reflect(bind=engine)


    Base = automap_base()
    Base.prepare(engine, reflect=True)

    DB.create_tables(engine, meta)


if __name__ == '__main__':
    create_new_database(CONFIG_FILE, CONFIG_SECTION)
    create_tables(CONFIG_FILE, CONFIG_SECTION)
