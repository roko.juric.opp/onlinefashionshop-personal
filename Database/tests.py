from werkzeug.datastructures import ImmutableDict

from Database.db_api import *
import unittest


class Test(unittest.TestCase):

    def test_login1(self):
        user = {"email": "Ivo.ivic@gmail.com",
                "firstName": "Ivo",
                "lastName": "Ivic",
                "address": "Adresa1",
                "password": "$argon2i$v=19$m=16,t=2,p=1$clhuRnoyWGdrSmc3T3BUTA$t5B27KliVjOtwJ9tscwelw"}
        post_user(user)
        data,error=validate_login("Ivo.ivic@gmail.com", "$argon2i$v=19$m=16,t=2,p=1$clhuRnoyWGdrSmc3T3BUTA$t5B27KliVjOtwJ9tscwelw")
        self.assertTrue(data)

    def test_login2(self):
        data,error=validate_login("pero.peric@gmail.com","$argon2i$v=19$m=16,t=2,p=1$clhuRnoyWGdrSmc3T3BUTA$t5B27KliVjOtwJ9tscwelw")
        self.assertTrue(error)

    def test_login3(self):
        user = {"email": "Ivo.ivic@gmail.com",
                "firstName": "Ivo",
                "lastName": "Ivic",
                "address": "Adresa1",
                "password": "$argon2i$v=19$m=16,t=2,p=1$clhuRnoyWGdrSmc3T3BUTA$t5B27KliVjOtwJ9tscwelw"}
        data,error=get_userId_by_data(user)
        delete_user(data)
        data2,error2=validate_login("Ivo.ivic@gmail.com","$argon2i$v=19$m=16,t=2,p=1$clhuRnoyWGdrSmc3T3BUTA$t5B27KliVjOtwJ9tscwelw")
        self.assertEqual('User is inactive',error2)


    def test_product1(self):
        product = {"product_name": "Šarena haljina",
                   "product_price": 400,
                   "desc": "šarena haljina",
                   "amount": {"S": 1, "M": 2, "L": 3},
                   "category_name": "Ostalo"
                   }
        file="sarenahaljina.jpg"
        post_product(product,file)

        data,error=get_productid_by_all_data(product)
        self.assertTrue(data)
        delete_product(data)

    def test_product2(self):
        product = {"product_name": "Šarena haljina",
                   "product_price": 400,
                   "desc": "šarena haljina",
                   "amount": {"S": 1, "M": 2, "L": 3},
                   "category_name": "Ostalo"
                   }
        productId,error =get_productid_by_all_data(product)
        data,error=get_products(productId)
        self.assertEqual(1,int(data[0]["amount"]["S"]))


    def test_product3(self):
        product = {"product_name": "crno-zelema haljina",
                   "product_price": 10000,
                   "desc": "lijepa crno-zelena haljina",
                   "category_name": "Ostalo"
                   }
        data,error=get_productid_by_all_data(product)
        self.assertFalse(data)



    def test_delete(self):
        user = {"email": "Ivo.ivic@gmail.com",
                "firstName": "Ivo",
                "lastName": "Ivic",
                "address": "Adresa1",
                "password": "$argon2i$v=19$m=16,t=2,p=1$clhuRnoyWGdrSmc3T3BUTA$t5B27KliVjOtwJ9tscwelw"}
        data, error = get_userId_by_data(user)
        data2,error2=perm_delete_user(data)
        self.assertFalse(error2)


if __name__ == '__main__':
    unittest.main()


