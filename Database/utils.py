import os

def preload_data(session, table, raw_data_file):
    existing_data = [i for i in session.execute(table.select())]

    if not existing_data:
        with open(os.path.join("/PreloadData", raw_data_file), "r", encoding="utf-8-sig") as f:
            raw_data = eval(f.read())
        insert_statement = table.insert().values(raw_data)

        session.execute(insert_statement)
        session.commit()


def preload_data_raw_sql(session, table, raw_sql_file):
    existing_data = [i for i in session.execute(table.select())]

    if not existing_data:
        # print(os.path.)
        #with open(os.path.join(".PreloadData", raw_sql_file), "r", encoding='utf-8-sig') as f:
        with open(os.path.join("PreloadData", raw_sql_file), "r", encoding='utf-8-sig') as f:
             raw_sql = f.read()

        session.execute(raw_sql)
        session.commit()