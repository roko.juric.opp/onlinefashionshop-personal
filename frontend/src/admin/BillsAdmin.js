import React, {Component} from 'react';
import BillsListAdmin from "./admin_components/BillsListAdmin";
import {Col, Container, Row} from "react-bootstrap";
import Bar from "./Bar";


class BillsAdmin extends Component {
    render() {
        return (
            <div style={{paddingTop:"3rem", marginTop: "1rem", backgroundColor:"#fafcfc"}}>
                <Container fluid>
                    <Row>
                        <Bar/>
                        <Col>
                             <BillsListAdmin/>
                        </Col>
                    </Row>
                </Container>



            </div>
        );
    }
}

export default BillsAdmin;