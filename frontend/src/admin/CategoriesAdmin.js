import React, { Component } from "react";
import { Button, Col, Container, Form, Modal, Row } from "react-bootstrap";
import CategoryListAdmin from "./admin_components/CategoryListAdmin";
import Bar from "./Bar";
import Fab from "@material-ui/core/Fab";
import Tooltip from "@material-ui/core/Tooltip";
import Axios from "axios";

class CategoriesAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      showAdd: false,
    };
  }
  handleChange = (event) => {
    this.setState({ name: event.target.value });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");

    Axios.post(
      window.backend + "/categories",
      {
        category_name: this.state.name,
      },
      { headers: { Authorization: `Bearer ${token}` } }
    )
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      })
      .catch((err) => console.log(err));
  };

  render() {
    const handleCloseAdd = () => this.setState({ showAdd: false });
    const handleShowAdd = () => this.setState({ showAdd: true });
    return (
      <div
        style={{
          paddingTop: "3rem",
          marginTop: "1rem",
          backgroundColor: "#fafcfc",
        }}
      >
        <Container fluid>
          <Row>
            <Bar />
            <Col>
              <Tooltip
                onClick={handleShowAdd}
                title="Add new category!"
                aria-label="add"
                style={{ backgroundColor: "#88b8e7", marginTop: "1rem" }}
              >
                <Fab color={"primary"} className="class">
                  <i className="fa fa-plus" aria-hidden="true">
                    {" "}
                  </i>
                </Fab>
              </Tooltip>
              <CategoryListAdmin />
            </Col>
          </Row>
        </Container>
        <Modal
          show={this.state.showAdd}
          onHide={handleCloseAdd}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton onClick={handleCloseAdd}>
            <Modal.Title>Add Category</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicAmount">
                <Form.Label>Category name: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter name"
                  onChange={this.handleChange}
                />
              </Form.Group>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              onClick={handleCloseAdd}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              Cancel
            </Button>
            <Button
              onClick={this.handleSubmit}
              style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
            >
              Add
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default CategoriesAdmin;
