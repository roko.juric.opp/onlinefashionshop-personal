import React, {Component} from "react";
import {Col, Container, Row} from "react-bootstrap";
import Bar from "./Bar";
import InfoListAdmin from "./admin_components/InfoListAdmin";


class InfoAdmin extends Component {

    render() {

        return (
            <div style={{paddingTop:"3rem", marginTop: "1rem", backgroundColor:"#fafcfc"}}>
                <Container fluid>
                    <Row>
                        <Bar/>
                        <Col>
                            <InfoListAdmin/>
                        </Col>
                    </Row>
                </Container>
            </div>
        )
    }




}

export default InfoAdmin;