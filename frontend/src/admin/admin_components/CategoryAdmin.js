import React, { Component } from "react";
import { Card, Button, Modal } from "react-bootstrap";
import "./css/Align.css";
import axios from "axios";

class CategoryAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDelete: false,
    };
  }

  handleSumbit = (event, id) => {
    event.preventDefault();
    const token = localStorage.getItem("token");
    axios
      .delete(window.backend + "/categories/" + id, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      });
  };
  render() {
    const handleCloseDelete = () => this.setState({ showDelete: false });
    const handleShowDelete = () => this.setState({ showDelete: true });
    return (
      <>
        <div className="izmedu">
          <Card style={{ width: "13rem" }} key={this.props.productId}>
            <Card.Text>ID: {this.props.categoryId}</Card.Text>
            <Card.Text>Name: {this.props.category_name}</Card.Text>
            <Button
              onClick={handleShowDelete}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              Delete
            </Button>
          </Card>
        </div>
        <Modal
          show={this.state.showDelete}
          onHide={handleShowDelete}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton onClick={handleCloseDelete}>
            <Modal.Title>
              Delete Category ID {this.props.categoryId}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Are you sure you want to delete this category?
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={handleCloseDelete}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              No
            </Button>
            <Button
              onClick={(e) => this.handleSumbit(e, this.props.categoryId)}
              style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
            >
              Yes
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default CategoryAdmin;
