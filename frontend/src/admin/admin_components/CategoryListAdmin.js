import React, { Component } from "react";
import CategoryAdmin from "./CategoryAdmin";
import axios from "axios";

class CategoryListAdmin extends Component {
  state = {
    categoryList: [],
  };
  componentDidMount() {
    axios.get(window.backend + "/categories").then((response) => {
      this.setState({
        categoryList: response.data["data"],
      });
    });
  }

  render() {
    return (
      <div>
        {this.state.categoryList.map((category) => {
          return (
            <CategoryAdmin
              categoryId={category.id}
              category_name={category.category_name}
              key={category.id}
            />
          );
        })}
      </div>
    );
  }
}

export default CategoryListAdmin;
