import React, { Component } from "react";
import { Accordion, Button, Card, Modal } from "react-bootstrap";
import "./css/Align.css";
import axios from "axios";

class CouponAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDelete: false,
    };
  }
  handleSumbit = (event, id) => {
    event.preventDefault();
    const token = localStorage.getItem("token");
    axios
      .delete(window.backend + "/coupons/" + id, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      });
  };
  render() {
    const handleCloseDelete = () => this.setState({ showDelete: false });
    const handleShowDelete = () => this.setState({ showDelete: true });
    return (
      <>
        <div className="izmedu">
          <Card style={{ width: "13rem" }} key={this.props.billId}>
            Coupon ID {this.props.id}
            <Accordion defaultActiveKey="0">
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="1">
                  Amount
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="1">
                  <Card.Body>{this.props.amount}</Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="2">
                  Code
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="2">
                  <Card.Body>{this.props.code}</Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="3">
                  Expires
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="3">
                  <Card.Body>{this.props.expires}</Card.Body>
                </Accordion.Collapse>
              </Card>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="4">
                  Percentage
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="4">
                  <Card.Body>{this.props.perc}</Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
            <Button
              onClick={handleShowDelete}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              Delete
            </Button>
          </Card>
        </div>
        <Modal
          show={this.state.showDelete}
          onHide={handleShowDelete}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton onClick={handleCloseDelete}>
            <Modal.Title>Delete Coupon ID {this.props.id}</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure you want to delete this coupon?</Modal.Body>
          <Modal.Footer>
            <Button
              onClick={handleCloseDelete}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              No
            </Button>
            <Button
              onClick={(e) => this.handleSumbit(e, this.props.id)}
              style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
            >
              Yes
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default CouponAdmin;
