import React, { Component } from "react";
import axios from "axios";
import Card from "./CouponAdmin";

class CouponsListAdmin extends Component {
  state = {
    couponsList: [],
  };
  componentDidMount() {
    axios.get(window.backend + "/coupons").then((response) => {
      this.setState({
        couponsList: response.data["data"],
      });
    });
  }

  render() {
    return (
      <div>
        {this.state.couponsList.map((coupon) => {
          return (
            <Card
                key={coupon}
              amount={coupon.amount}
              code={coupon.code}
              id={coupon.coupon_id}
              expires={coupon.expires}
              perc={coupon.percentage}
            />
          );
        })}
      </div>
    );
  }
}

export default CouponsListAdmin;
