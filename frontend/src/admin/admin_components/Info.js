import {Alert} from "react-bootstrap";
import React from "react";

function Info(props) {
    return (
        <div className="izmedu">
            <Alert variant="info"> Id: {props.id}</Alert>
            <Alert variant="info"> First name: {props.firstName}</Alert>
            <Alert variant="info"> Last name: {props.lastName}</Alert>
            <Alert variant="info"> Email: {props.email}</Alert>
            <Alert variant="info"> Role: {props.role}</Alert>
        </div>

    )
}

export default Info;