import React, { Component } from "react";
import { Card, Button, Modal } from "react-bootstrap";
import "./css/Align.css";
import axios from "axios";

class Owner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showDelete: false,
    };
  }
  handleDelete = (event) => {
    event.preventDefault();
    const token = localStorage.getItem("token");
    axios
      .delete(window.backend + "/users/" + this.props.ownerId, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      });
  };

  render() {
    if (
      localStorage.getItem("token") != null &&
      localStorage.getItem("role") === "SystemAdmin"
    ) {
      const handleCloseDelete = () => this.setState({ showDelete: false });
      const handleShowDelete = () => this.setState({ showDelete: true });

            return (
                <>
                    <div className="izmedu">
                        { this.props.role === "Owner" &&
                            (<Card style={{width: "13rem"}} key={this.props.ownerId}>
                                <Card.Text>ID: {this.props.ownerId}</Card.Text>
                                <Card.Text>First name: {this.props.firstName}</Card.Text>
                                <Card.Text>Last name: {this.props.lastName}</Card.Text>
                                <Card.Text>Role: {this.props.role}</Card.Text>
                                {localStorage.getItem("token") != null &&
                                localStorage.getItem("role") === "SystemAdmin" && (
                                    <Button onClick={handleShowDelete}
                                            style={{backgroundColor: "#e06666", borderColor: "#e06666"}}>
                                        Delete
                                    </Button>
                                )}
                            </Card>
                            )}
                    </div>
                    <Modal
                        show={this.state.showDelete}
                        onHide={handleShowDelete}
                        backdrop="static"
                        keyboard={false}
                        centered
                    >
                        <Modal.Header closeButton onClick={handleCloseDelete}>
                            <Modal.Title>Delete Owner ID {this.props.ownerId}</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Are you sure you want to delete this owner?
                        </Modal.Body>
                        <Modal.Footer>
                            <Button variant="secondary" onClick={handleCloseDelete}
                                    style={{backgroundColor: "#e06666", borderColor: "#e06666"}}>
                                No
                            </Button>
                            <Button onClick={(e) =>this.handleDelete(e)} style={{backgroundColor: "#88b8e7", borderColor: "#88b8e7"}}>Yes</Button>
                        </Modal.Footer>
                    </Modal>

                </>

            )
        } else{
            return(
                <div className="izmedu">
                    {this.props.role === "BasicUser" && (
                    <Card style={{width: "13rem"}} key={this.props.ownerId}>
                        <Card.Text>ID: {this.props.ownerId}</Card.Text>
                        <Card.Text>First name: {this.props.firstName}</Card.Text>
                        <Card.Text>Last name: {this.props.lastName}</Card.Text>
                        <Card.Text>Role: {this.props.role}</Card.Text>
                    </Card>
                    )}
                </div>
            )
        }
    }


}
export default Owner;