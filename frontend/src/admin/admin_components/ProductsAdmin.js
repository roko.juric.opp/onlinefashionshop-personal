import React, { Component } from "react";
import { Button, Card, Col, Form, Modal } from "react-bootstrap";
import "./css/Align.css";
import axios from "axios";
import Axios from "axios";
import { Row } from "reactstrap";
import RemoveIcon from "@material-ui/icons/Remove";
import AddIcon from "@material-ui/icons/Add";

class ProductsAdmin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.productName,
      description: this.props.description == null ? "" : this.props.description,
      price: this.props.productPrice,
      size: "",
      img: this.props.imgUrl,
      showDelete: false,
      showUpdate: false,
      amountS: this.props.amount_S == null ? 0 : this.props.amount_S,
      amountM: this.props.amount_M == null ? 0 : this.props.amount_M,
      amountL: this.props.amount_L == null ? 0 : this.props.amount_L,
      selectedFile: null,
    };
  }

  handleChangeName = (event) => {
    this.setState({ name: event.target.value });
  };

  handleChangeDesc = (event) => {
    this.setState({ description: event.target.value });
  };

  handleChangePrice = (event) => {
    this.setState({ price: event.target.value });
  };

  handleChangeSize = (event) => {
    this.setState({ size: event.target.value });
  };

  handleChangeImg = (event) => {
    this.setState({ img: event.target.value });
  };

  handleDelete = (event, id) => {
    event.preventDefault();

    const token = localStorage.getItem("token");
    axios
      .delete(window.backend + "/products/" + id, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      });
  };

  getFormData(object) {
    const formData = new FormData();
    Object.keys(object).forEach((key) => formData.append(key, object[key]));
    return formData;
  }

  handleUpdate = (event, id) => {
    event.preventDefault();

    const data = this.getFormData({
      amount_L: this.state.amountL,
      amount_M: this.state.amountM,
      amount_S: this.state.amountS,
      category_id: this.props.categoryId,
      category_name: this.props.categoryName,
      is_active: this.props.categoryIsActive,
      desc: this.state.description,
      id: id,
      product_name: this.state.name,
      product_price: this.state.price,
      file: this.state.selectedFile == null ? null : this.state.selectedFile,
    });

    for (let pair of data.entries()) {
      console.log(pair[0] + ", " + pair[1]);
    }

    Axios.put(window.backend + "/products/" + id, data, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
        ContentType: "multipart/form-data",
      },
    })
      .then((res) => {
        console.log(res);
        this.setState({ showDelete: false });
        window.location.reload(false);
      })
      .catch((err) => console.log(err));
  };

  render() {
    const handleCloseDelete = () => this.setState({ showDelete: false });
    const handleShowDelete = () => this.setState({ showDelete: true });
    const handleCloseUpdate = () => this.setState({ showUpdate: false });
    const handleShowUpdate = () => this.setState({ showUpdate: true });
    return (
      <>
        <div className="izmedu">
          <Card style={{ width: "13rem" }} key={this.props.productId}>
            <Card.Text>ID: {this.props.productId}</Card.Text>
            <Card.Text>Name: {this.props.productName}</Card.Text>
            <Card.Text>Price: {this.props.productPrice}</Card.Text>
            <Card.Text>Description: {this.props.description}</Card.Text>
            <Card.Text>Category: {this.props.categoryName}</Card.Text>

            {this.state.amountS !== null && (
              <Card.Text>Size S: {this.state.amountS}</Card.Text>
            )}
            {this.state.amountM !== null && (
              <Card.Text>Size M: {this.state.amountM}</Card.Text>
            )}
            {this.state.amountL !== null && (
              <Card.Text>Size L: {this.state.amountL}</Card.Text>
            )}

            {this.state.amountS === null && <Card.Text>Size S: - </Card.Text>}
            {this.state.amountM === null && <Card.Text>Size M: - </Card.Text>}
            {this.state.amountL === null && <Card.Text>Size L: - </Card.Text>}

            <Card.Body>
              <Card.Img
                src={this.props.imgURL}
                style={{ width: "10rem", height: "13rem" }}
              />
            </Card.Body>
            <Card.Footer>
              <Button
                onClick={handleShowUpdate}
                style={{ backgroundColor: "#b0cebc", borderColor: "#b0cebc" }}
              >
                Update
              </Button>
              <Button
                onClick={handleShowDelete}
                style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
              >
                Delete
              </Button>
            </Card.Footer>
          </Card>
        </div>
        <Modal
          show={this.state.showDelete}
          onHide={handleCloseDelete}
          backdrop="static"
          keyboard={false}
          centered
        >
          <Modal.Header closeButton onClick={handleCloseDelete}>
            <Modal.Title>Delete Product ID {this.props.productId}</Modal.Title>
          </Modal.Header>
          <Modal.Body>Are you sure you want to delete this product?</Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={handleCloseDelete}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              No
            </Button>
            <Button
              onClick={(e) => this.handleDelete(e, this.props.productId)}
              style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
            >
              Yes
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          show={this.state.showUpdate}
          onHide={handleCloseUpdate}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton onClick={handleCloseDelete}>
            <Modal.Title>Update Product ID {this.props.productId}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form>
              <Form.Group controlId="formBasicName">
                <Form.Label>Product name: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter product name"
                  defaultValue={this.state.name}
                  onChange={this.handleChangeName}
                />
              </Form.Group>
              <Form.Group controlId="formBasicDescription">
                <Form.Label>Description: </Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="Enter description"
                  defaultValue={this.state.description}
                  onChange={this.handleChangeDesc}
                />
              </Form.Group>
              <Form.Group controlId="formBasicPrice">
                <Form.Label>Price: </Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter price"
                  defaultValue={this.state.price}
                  onChange={this.handleChangePrice}
                />
              </Form.Group>
              <Form.Group className="size_box">
                <Row>
                  <Card.Text style={{ marginLeft: "5%" }}>
                    Size S: {this.state.amountS}
                  </Card.Text>
                  <Col>
                    <Button
                      onClick={() => {
                        this.state.amountS > 0
                          ? this.setState({ amountS: this.state.amountS - 1 })
                          : this.setState({
                              amountS: this.state.amountS,
                            });
                      }}
                      variant="danger"
                      size="sm"
                      style={{
                        backgroundColor: "#e06666",
                        borderColor: "#e06666",
                        marginLeft: "20%",
                      }}
                    >
                      <RemoveIcon />
                    </Button>
                    <Button
                      onClick={() => {
                        this.setState({ amountS: this.state.amountS + 1 });
                      }}
                      variant="success"
                      size="sm"
                      style={{
                        backgroundColor: "#b0cebc",
                        borderColor: "#b0cebc",
                      }}
                    >
                      <AddIcon />
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Card.Text style={{ marginLeft: "5%" }}>
                    Size M: {this.state.amountM}
                  </Card.Text>
                  <Col>
                    <Button
                      onClick={() => {
                        this.state.amountM > 0
                          ? this.setState({ amountM: this.state.amountM - 1 })
                          : this.setState({
                              amountM: this.state.amountM,
                            });
                      }}
                      variant="danger"
                      size="sm"
                      style={{
                        backgroundColor: "#e06666",
                        borderColor: "#e06666",
                        marginLeft: "19%",
                      }}
                    >
                      <RemoveIcon />
                    </Button>
                    <Button
                      onClick={() => {
                        this.setState({ amountM: this.state.amountM + 1 });
                      }}
                      variant="success"
                      size="sm"
                      style={{
                        backgroundColor: "#b0cebc",
                        borderColor: "#b0cebc",
                      }}
                    >
                      <AddIcon />
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Card.Text style={{ marginLeft: "5%" }}>
                    Size L: {this.state.amountL}
                  </Card.Text>
                  <Col>
                    <Button
                      onClick={() => {
                        this.state.amountL > 0
                          ? this.setState({ amountL: this.state.amountL - 1 })
                          : this.setState({
                              amountL: this.state.amountL,
                            });
                      }}
                      variant="danger"
                      size="sm"
                      style={{
                        backgroundColor: "#e06666",
                        borderColor: "#e06666",
                        marginLeft: "20%",
                      }}
                    >
                      <RemoveIcon />
                    </Button>
                    <Button
                      onClick={() => {
                        this.setState({ amountL: this.state.amountL + 1 });
                      }}
                      variant="success"
                      size="sm"
                      style={{
                        backgroundColor: "#b0cebc",
                        borderColor: "#b0cebc",
                      }}
                    >
                      <AddIcon />
                    </Button>
                  </Col>
                </Row>
              </Form.Group>
              <div>
                <div>
                  <input
                    type="file"
                    onChange={(event) =>
                      this.setState({ selectedFile: event.target.files[0] })
                    }
                  />
                </div>
              </div>
            </Form>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={handleCloseUpdate}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              Cancel
            </Button>
            <Button
              onClick={(e) => this.handleUpdate(e, this.props.productId)}
              style={{ backgroundColor: "#88b8e7", borderColor: "#88b8e7" }}
            >
              Update
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
}

export default ProductsAdmin;
