import React, { Component } from "react";
import BasketCard from "./BasketCard";
import axios from "axios";
import EmptyBasket from "./EmptyBasket";
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";
import ForwardIcon from "@material-ui/icons/Forward";
import RotateRightIcon from "@material-ui/icons/RotateRight";

class BasketComp extends Component {
  state = {
    basketState: [],
    loading: true,
  };

  componentDidMount() {
    const api = window.backend + "/baskets/1";
    const token = localStorage.getItem("token");
    axios
      .get(api, { headers: { Authorization: `Bearer ${token}` } })
      .then((res) => {
        this.setState({ loading: false });
        this.setState({ basketState: res.data["data"] });
      })
      .catch((err) => console.log(err));
  }

  render() {
    if (this.state.loading) {
      return (
        <div
          style={{
            backgroundColor: "#e8f4fc",
            padding: "10%",
            textAlign: "center",
            fontSize: "3vw",
            fontStyle: "italic",
          }}
        >
          <RotateRightIcon style={{ fontSize: "3vw" }} />
          Loading...
        </div>
      );
    }
    console.log(this.state.basketState);
    console.log(Object.keys(this.state.basketState).length !== 0);
    let count = 0;
    for (let i = 0; i < Object.keys(this.state.basketState).length; i++) {
      count +=
        this.state.basketState[i].amount *
        this.state.basketState[i].product_price;
    }
    return (
      <div style={{ backgroundColor: "#e8f4fc", paddingTop: "2rem", paddingBottom:"2rem" }}>
        {this.state.basketState.map((b) => {
          return (
            <div key={b}>
              {Object.keys(this.state.basketState).length !== 0 && (
                <BasketCard
                  amount={b.amount}
                  basketId={b.basket_id}
                  imgURL={b.img_url}
                  productId={b.product_id}
                  productName={b.product_name}
                  productPrice={b.product_price}
                  sizeId={b.size_id}
                  sizeName={b.size_name}
                  userBasketId={b.user_basket_id}
                  key={b.basket_id}
                />
              )}
            </div>
          );
        })}
        <div>
          {Object.keys(this.state.basketState).length === 0 && <EmptyBasket />}
        </div>
        {Object.keys(this.state.basketState).length !== 0 && (
          <div
            style={{
              backgroundColor: "#d9e0d9",
              padding: "2%",
              marginLeft: "0.5%",
              marginRight: "0.5%",
              borderLeft: "1px solid darkolivegreen",
              borderRight: "1px solid darkolivegreen",
              borderBottom: "1px solid darkolivegreen",
            }}
          >
            <Row>
              <Col xs={6}/>
              <Col md={{ span: 2, offset: 10 }}>
                <strong>Total price: {count} HRK</strong>
              </Col>
            </Row>
            <Row>
              <Col xs={6}/>
              <Col md={{ span: 2, offset: 10 }}>
                <strong>
                  <ForwardIcon /> Go to{" "}
                  <Link to="/baskets/checkout" style={{ color: "#2f4c4f" }}>
                    {" "}
                    checkout{" "}
                  </Link>
                </strong>
              </Col>
            </Row>
          </div>
        )}
      </div>
    );
  }
}

export default BasketComp;
