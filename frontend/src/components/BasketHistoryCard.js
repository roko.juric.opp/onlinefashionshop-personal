import React from "react";
import {Button, Card, Row, Col} from "react-bootstrap";
import {Link} from "react-router-dom";
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet';


function BasketHistoryCard(props) {
  console.log(props + " props u card");
  console.log(props.sum + " sum u card");

  return (
    <div style ={{
        marginTop: "3%",
        marginBottom: "3%",
        marginLeft: "3%",
        display: "inline-block",
    }}
    >
      <Card
          style={{
              width: "18rem",
              backgroundColor: "whitesmoke"
          }}
          key={props.userBasketId}
      >
          <Button
              style={{
                backgroundColor: "#a6b8a6",
                opacity: "1",
                textAlign: "center",
                borderColor:"#a6b8a6",
              }}
              disabled
          >
            <Card.Title style={{
              paddingTop:"3%"
            }} >
              <SubdirectoryArrowRightIcon
                  style={{
                    color: "darkolivegreen",
                  }}
              />
              <Link to={"/baskets/unactive/" + props.userBasketId}
                    style={{
                      color: "darkolivegreen",
                      textDecoration: "underline",

              }}
              >
                Basket: {props.userBasketId}
              </Link>
              <ShoppingBasketIcon
                  style={{
                    textAlign: "right",
                    color: "darkolivegreen",
                  }}
              />
            </Card.Title>

          </Button>

          <Card.Body>
              <Row>
                  <Col>
                      <AccountBalanceWalletIcon
                          style={{
                              textAlign:"left",
                              color:"darkolivegreen",
                          }}
                      />
                  </Col>
                  <Col xs={7}>
                      <Card.Text
                          style={{
                              textAlign:"right"
                          }}
                      >
                          Total price: {props.sum} HRK
                      </Card.Text>
                  </Col>

              </Row>

          </Card.Body>
      </Card>
    </div>
  );
}

export default BasketHistoryCard;
