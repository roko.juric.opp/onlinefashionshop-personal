import React, { Component } from 'react';
import {Button, Card} from "react-bootstrap";
import {Link} from "react-router-dom";
import './css/CollectionShow.css'

class CollectionShow extends Component {

    render() {
        return (
            <div className="collectionbackground"
                style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                padding: "7%",
                backgroundColor:"#e8f4fc",
            }}
            >
                <Card className="collection" style={{
                    paddingLeft: "7%",
                    paddingRight: "7%",
                    border: "none",
                    boxShadow: "none",
                    borderRadius: "0",
                    backgroundColor: "#fbdde9"
                }}>
                    <Card.Header style={{
                        backgroundColor: "white",
                        display: "flex",
                        justifyContent: "center",
                        padding: "0",
                        fontWeight: "bold",
                        color: "#2f4c4f",
                        fontStyle: "oblique"
                    }}
                    >
                        <Card.Text style={{

                        }}>
                            GREENTEAM WEBSHOP
                        </Card.Text>
                    </Card.Header>
                    <Card.Body>
                        <Card.Text style={{
                            textAlign: "center",
                            fontWeight: "bold",
                            fontSize: "6vw",
                            marginBottom: "0",
                        }}
                        >
                            NEW
                        </Card.Text>
                        <Card.Text style={{
                            textAlign: "center",
                            fontSize: "3vw",
                            marginTop: "0",
                        }}>COLLECTION</Card.Text>
                        <Card.Text style={{
                            textAlign: "center",
                            fontSize: "1.5vw",
                        }}
                        >
                             Collection {(new Date().getFullYear()-1)}./{(new Date().getFullYear())}. out now.
                        </Card.Text>
                        <Card.Text style={{
                            fontSize: "1.5vw",
                            display: "flex",
                            justifyContent: "center",
                        }}>
                            <Link to="/products">
                                <Button variant="dark"
                                style={{backgroundColor: "#2f4c4f"}}>See more!</Button>
                            </Link>
                        </Card.Text>
                    </Card.Body>


                </Card>

            </div>
        );
    }
}

export default CollectionShow;