import React, { Component } from "react";
import {Card, Container, Image} from "react-bootstrap";
import slika from "../images/coupon.jpg";
import "./css/Coupon.css";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';

class CouponCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isClicked: false,
    };
  }

  render() {
    return (
          <div
               style={{
                   position: "relative",
                   top: "0",
                   left: "0",
          }}>
            <Image className="coupon_show" src={slika}
                   title="Click me!"
                   onClick={() => this.setState({ isClicked: true })}
                   style={{
                     position: "relative",
                   }}
                   height={300}
                   width={300}



            />
            {this.state.isClicked && (
                <Container fluid>
                  <Card
                      style={{
                        position: "absolute",
                        top: "5%",
                        left: "5%",
                        width:"10rem",
                        backgroundColor: "#fdeef4"
                      }}
                  >
                    <Card.Body>
                      <Card.Title style={{
                          fontSize: "1vw",
                      }}>


                      </Card.Title>
                      <Card.Subtitle style={{
                          fontSize: "1vw",
                      }}>
                        {this.props.code } {" "}
                          {this.props.percentage}%
                          <HighlightOffIcon
                              onClick={() => this.setState({ isClicked: false })}
                              style={{
                                  float:"right",
                              }}
                          />

                      </Card.Subtitle>
                    </Card.Body>
                  </Card>
                </Container>

            )}
          </div>

    );
  }
}
export default CouponCard;
