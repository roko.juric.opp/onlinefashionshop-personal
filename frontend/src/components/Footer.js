import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <div
        style={{
          color: "white",
          background: "#2f4c4f",
          paddingTop: "3em",
          position: "relative",
          bottom: "0",
          width: "100%",
          // eslint-disable-next-line
          position: "static",
        }}
      >
        <div className="container">
          <div className="row">
            {/* Column1 */}
            <div className="col">
              <h4>Details</h4>
              <ul className="list-unstyled">
                <li>Help</li>
                <li>Info</li>
              </ul>
            </div>
            {/* Column2 */}
            <div className="col">
              <h4>More info</h4>
              <ul className="list-unstyled">
                <li>Delivery</li>
                <li>Payment</li>
                <li>Return policy</li>
              </ul>
            </div>
            {/* Column3 */}
          </div>
          <hr />
          <div className="row">
            <p className="col-sm">
              &copy;{new Date().getFullYear()} Green Team | All rights reserved
              | Terms Of Service | Privacy
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
