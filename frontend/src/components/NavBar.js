import React, {Component} from "react";

import "font-awesome/css/font-awesome.min.css";
import SearchBar from "./SearchBar";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import BasketIcon from "./BasketIcon";
import axios from "axios";
import SettingsIcon from "@material-ui/icons/Settings";
import PersonIcon from "@material-ui/icons/Person";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import ViewModuleIcon from "@material-ui/icons/ViewModule";
import EcoIcon from "@material-ui/icons/Eco";
import BusinessCenterIcon from "@material-ui/icons/BusinessCenter";
import CreateIcon from "@material-ui/icons/Create";
import LockIcon from "@material-ui/icons/Lock";
import LocalOfferIcon from "@material-ui/icons/LocalOffer";

class NavBar extends Component {
  state = {
    categoriesList: [],
  };


  loadCategories() {
    axios.get(window.backend + "/categories").then((response) => {
      this.setState({
        categoriesList: response.data["data"],
      });
    });
  }

  logout() {
    localStorage.clear();
  }

  render() {
    const firstName = localStorage.getItem("first_name");
    const lastName = localStorage.getItem("last_name");
    const style = {
      backgroundColor: "#2f4c4f",
      height: "1%",
      width: "100%",
      position: "fixed",
      top: "0",
      opacity: 0.95,
      fontSize: "0.9em",
    };
    return (
      <Navbar collapseOnSelect expand="lg" variant="dark" style={style}>
        {/*<Navbar.Brand href="/home">Home<i className="fa fa-leaf"></i></Navbar.Brand>*/}
        <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto" style={{backgroundColor: "#2f4c4f"}}>
            <Nav.Link href="/" style={{ marginLeft: "0px"}}>
              Home
              <EcoIcon />
            </Nav.Link>
            <NavDropdown
              title="Categories"
              id="collasible-categories-dropdown"
              style={{ marginLeft: "10px" }}
              onClick={() => this.loadCategories()}
            >
              {this.state.categoriesList.map((category) => {
                return (
                  <NavDropdown.Item
                    href={"/categories/" + category.id}
                    key={category.id}
                  >
                    {category.category_name}
                  </NavDropdown.Item>
                );
              })}
              <NavDropdown.Divider />
              <NavDropdown.Item href="/categories">
                All Categories <ViewModuleIcon style={{ opacity: "0.3" }} />
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link href="/products">
              Products <LocalOfferIcon />{" "}
            </Nav.Link>{" "}
            <SearchBar />
          </Nav>
          <Nav style={{backgroundColor: "#2f4c4f"}}>
            {localStorage.getItem("token") != null && (
              <Nav.Link href="/profile">
                {firstName} {lastName} <PersonIcon />
              </Nav.Link>
            )}
            {localStorage.getItem("token") == null && (
              <Nav.Link href="/login">
                Login <LockIcon />{" "}
              </Nav.Link>
            )}
            {localStorage.getItem("token") == null && (
              <Nav.Link href="/registration">
                Registration <CreateIcon />{" "}
              </Nav.Link>
            )}
            <BasketIcon />

            {localStorage.getItem("token") != null &&
              localStorage.getItem("role") === "SystemAdmin" && (
                <Nav.Link href="/admin">
                  Admin page
                  <SettingsIcon />
                </Nav.Link>
              )}

            {localStorage.getItem("token") != null &&
              localStorage.getItem("role") === "Owner" && (
                <Nav.Link href="/admin">
                  Owner page <BusinessCenterIcon />
                </Nav.Link>
              )}
            {localStorage.getItem("token") != null && (
              <Nav.Link href="/" onClick={() => this.logout()}>
                Logout <ExitToAppIcon />
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default NavBar;
