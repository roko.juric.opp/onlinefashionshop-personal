import React, { Component } from "react";
import ProductDisplayCard from "./ProductDisplayCard";
import axios from "axios";
import {
  Form,
  Button,
  ButtonGroup,
  Col,
  Row,
  Card,
  Container,
} from "react-bootstrap";

class ProductDisplay extends Component {
  state = {
    productsList: [],
    categoriesList: [],
    minPrice: "",
    maxPrice: "",
    sizeL: false,
    sizeM: false,
    sizeS: false,
    categoryFilter: [],
  };

  componentDidMount() {
    axios
      .get(window.backend + "/products")
      .then((response) => {
        this.setState({ productsList: response.data["data"] });
        axios
          .get(window.backend + "/categories")
          .then((response2) => {
            this.setState({ categoriesList: response2.data["data"] });
            this.fillCategoryStateFilter();
          })

          .catch((err2) => console.log(err2));
      })
      .catch((err) => console.log(err));
  }

  loadProducts() {
    let productsForDisplay = [];
    let products = [];
    // eslint-disable-next-line
    this.state.productsList.map((product) => {
      if (
        this.state.minPrice <= product.product_price &&
        this.state.maxPrice === ""
      ) {
        products.push(product);
      } else if (
        this.state.minPrice <= product.product_price &&
        this.state.maxPrice !== "" &&
        this.state.maxPrice >= product.product_price
      ) {
        products.push(product);
      }
    });
    products = this.filterCategories(products);
    products = this.filterSizes(products);

    products.map((product) =>
      productsForDisplay.push(
        <ProductDisplayCard
          productId={product.id}
          productName={product.product_name}
          productPrice={product.product_price}
          imgURL={product.img_url}
          description={product.desc}
          sizeId={product.size_id}
          key={product.id}
        />
      )
    );

    return productsForDisplay;
  }

  filterCategories(products) {
    let productsForDisplay = [];
    let counter = 0;
    for (let i = 0; i < this.state.categoryFilter.length; i++) {
      if (this.state.categoryFilter[i] === true) counter++;
    }
    if (counter === 0) return products;

    for (let i = 0; i < products.length; i++) {
      const categoryId = products[i].category.category_id;
      if (this.state.categoryFilter[categoryId - 1])
        productsForDisplay.push(products[i]);
    }

    return productsForDisplay;
  }

  filterSizes(products) {
    let productsForDisplay = [];
    let i;
    for (i = 0; i < products.length; i++) {
      if (
        this.state.sizeL &&
        products[i].amount.L != null &&
        products[i].amount.L !== 0
      )
        productsForDisplay.push(products[i]);
      else if (
        this.state.sizeM &&
        products[i].amount.M != null &&
        products[i].amount.M !== 0
      )
        productsForDisplay.push(products[i]);
      else if (
        this.state.sizeS &&
        products[i].amount.S != null &&
        products[i].amount.S !== 0
      )
        productsForDisplay.push(products[i]);
      else if (!this.state.sizeL && !this.state.sizeM && !this.state.sizeS)
        productsForDisplay.push(products[i]);
    }

    return productsForDisplay;
  }

  loadCategoryFilters() {
    return this.state.categoriesList.map((category) => (
      <Button
        key={category.id}
        variant={
          this.state.categoryFilter[category.id - 1] ? "dark" : "outline-dark"
        }
        value={this.state.categoryFilter[category.id - 1]}
        onClick={() => {
          this.switchCategoryFilter(category.id - 1);
        }}
      >
        {category.category_name}
      </Button>
    ));
  }

  switchCategoryFilter(index) {
    let helperArray = [];
    for (let i = 0; i < this.state.categoryFilter.length; i++) {
      if (i !== index) helperArray.push(this.state.categoryFilter[i]);
      else helperArray.push(!this.state.categoryFilter[i]);
    }
    this.setState({ categoryFilter: helperArray });
  }

  fillCategoryStateFilter() {
    let array = [];
    for (let i = 0; i < this.state.categoriesList.length; i++)
      array.push(false);
    this.setState({ categoryFilter: array });
  }

  handleClear() {
    this.setState({ sizeL: false });
    this.setState({ sizeM: false });
    this.setState({ sizeS: false });
    this.setState({ minPrice: "" });
    this.setState({ maxPrice: "" });
    let helperArray = [];
    for (let i = 0; i < this.state.categoryFilter.length; i++)
      helperArray.push(false);
    this.setState({ categoryFilter: helperArray });
  }

  render() {
    return (
      <div style={{ backgroundColor: "#e8f4fc", paddingTop: "1.5%" }}>
        <div className="filter">
          <Container fluid>
            <Card style={{ backgroundColor: "#d9e0d9" }}>
              <Row>
                <Col md="auto" style={{ marginLeft: "2rem" }}>
                  <Form.Group controlId="formBasicText">
                    <Form.Label className="label">Prices:</Form.Label>
                    <Row>
                      <Col>
                        <Form.Control
                          className="textBoxMin"
                          type="text"
                          placeholder="Lowest price"
                          value={this.state.minPrice}
                          onChange={(event) => {
                            if (!isNaN(event.target.value))
                              this.setState({ minPrice: event.target.value });
                          }}
                        />
                      </Col>
                      <Col>
                        <Form.Control
                          className="textBoxMax"
                          type="text"
                          placeholder="Highest price"
                          value={this.state.maxPrice}
                          onChange={(event) => {
                            if (!isNaN(event.target.value))
                              this.setState({ maxPrice: event.target.value });
                          }}
                        />
                      </Col>
                    </Row>
                  </Form.Group>
                </Col>
                <Col md="auto">
                  <Form.Label className="label">Sizes:</Form.Label>
                  <ButtonGroup type="checkbox" className="mb-2">
                    <Button
                      variant={this.state.sizeS ? "dark" : "outline-dark"}
                      value={3}
                      onClick={() => {
                        if (!this.state.sizeS) this.setState({ sizeS: true });
                        else this.setState({ sizeS: false });
                      }}
                    >
                      S
                    </Button>
                    <Button
                      variant={this.state.sizeM ? "dark" : "outline-dark"}
                      value={2}
                      onClick={() => {
                        if (!this.state.sizeM) this.setState({ sizeM: true });
                        else this.setState({ sizeM: false });
                      }}
                    >
                      M
                    </Button>
                    <Button
                      variant={this.state.sizeL ? "dark" : "outline-dark"}
                      value={1}
                      onClick={() => {
                        if (this.state.sizeL === false)
                          this.setState({ sizeL: true });
                        else this.setState({ sizeL: false });
                      }}
                    >
                      L
                    </Button>
                  </ButtonGroup>
                </Col>

                <Col>
                  <Form.Label className="label">Categories:</Form.Label>
                  <ButtonGroup type="checkbox" className="mb-2">
                    {this.loadCategoryFilters()}
                  </ButtonGroup>
                </Col>

                <Col md={{ span: 0, offset: 1 }}>
                  <Button
                    variant="dark"
                    style={{
                      marginTop: "1.6rem",
                    }}
                    onClick={(event) => this.handleClear(event)}
                  >
                    Clear filters
                  </Button>
                </Col>
              </Row>
            </Card>
          </Container>
        </div>

        <div
          style={{
            marginLeft: "1rem",
            marginTop: "1.5rem",
            marginBottom: "1.5rem",
            display: "inline-block",
          }}
        >
          {this.loadProducts()}
        </div>
      </div>
    );
  }
}

export default ProductDisplay;
