import React, { Component } from "react";
import { Form, FormControl, Button } from "react-bootstrap";

class SearchBar extends Component {
  state = {
    query: "",
  };

  makeQuery() {
    let queryToSend = this.state.query;
    return "?data=" + queryToSend.trim().toLowerCase().replaceAll(" ", "+");
  }

  render() {
    return (
      <Form inline>
        <FormControl
          size="sm"
          type="text"
          placeholder="Search"
          value={this.state.query}
          onChange={(event) => {
            this.setState({ query: event.target.value });
          }}
          style={{height:"65%"}}
        />
        <Button
          size="sm"
          variant="outline-info"
          href={"/search" + this.makeQuery()}
          style={{height:"65%", display:"flex", alignItems:"center"}}
        >
          <i className="fa fa-search"> </i>
        </Button>
      </Form>
    );
  }
}

export default SearchBar;
