import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "bulma/css/bulma.min.css";
import "bootstrap/dist/css/bootstrap.min.css";

window.backend = "https://onlinefashionshop.herokuapp.com/";

ReactDOM.render(<App />, document.getElementById("root"));
