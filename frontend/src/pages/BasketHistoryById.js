import React, { Component } from "react";
import axios from "axios";
import { Row, Col, Media } from "react-bootstrap";
import {Link} from "react-router-dom";
import KeyboardReturnIcon from '@material-ui/icons/KeyboardReturn';

class BasketHistoryById extends Component {
  state = {
    userBasketState: [],
  };

  componentDidMount() {
    console.log("usao u comDidMount");
    const currentPath = window.location.pathname;
    const index = currentPath.lastIndexOf("/");
    const userBasketId = currentPath.slice(index + 1, currentPath.length);
    const token = localStorage.getItem("token");
    console.log(userBasketId);

    axios
      .get(window.backend + "/userbaskets/" + userBasketId, {
        headers: { Authorization: `Bearer ${token}` },
      })
      .then((response) => {
        this.setState({ userBasketState: response.data["data"] });
      })
      .catch((err) => console.log(err));
  }

  render() {
    console.log(this.state.userBasketState);
    const styles = {
      mediaItem: {

      },
    };
    return (
      <div>
        {this.state.userBasketState.map((b) => {
          return (
            <div style={{backgroundColor:"#e8f4fc", paddingTop:"1.5rem", paddingBottom: "0.3rem"}}>
              <Link to={"/baskets/unactive"}
                    style={{
                      marginLeft:"0.5%",
                      color:"black",
                      fontSize:"1.3vw",
              }}
              >
                <KeyboardReturnIcon/> Go back to basket history
              </Link>
              <Media className={styles.mediaItem}
                     style={{
                       backgroundColor: "whitesmoke",
                       marginLeft:"0.5%",
                       marginRight:"0.5%",
                       padding:"1%",
                       border: "1px solid darkolivegreen",
                     }}>
                <img
                  width={150}
                  height={150}
                  className="align-self-center mr-3"
                  src={b.img_url}
                  alt="Generic placeholder"
                />
                <Media.Body>
                  <p>
                    <strong>
                      <i style={{ fontSize: "2.5vw" }}>{b.product_name}</i>
                    </strong>
                  </p>
                  <Row>
                    <Col xs={6}>
                      <strong style={{ fontSize: "1.3vw" }}>
                        {b.amount} piece(s) in size {b.size_name}
                      </strong>
                    </Col>
                    <Col xs={6}>
                      <strong style={{ fontSize: "1.3vw" }}>
                        Price: {b.product_price * b.amount} HRK
                      </strong>
                    </Col>
                  </Row>
                </Media.Body>
              </Media>
            </div>
          );
        })}
      </div>
    );
  }
}
export default BasketHistoryById;
