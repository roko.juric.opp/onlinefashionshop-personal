import React from "react";
import CategoryComp from "../components/CategoryComp";

function Categories() {
  return (
    <div style={{backgroundColor:"#e8f4fc"}}>
      <CategoryComp />
    </div>
  );
}

export default Categories;
