import React, { Component } from "react";
import axios from "axios";
import ProductDisplayCard from "../components/ProductDisplayCard";
import {
  Form,
  ButtonGroup,
  Col,
  Row,
  Card,
  Button,
  Container,
  Badge,
  Alert,
} from "react-bootstrap";
import { Link } from "react-router-dom";

class CategoryById extends Component {
  state = {
    products: [],
    categoryName: "",
    loading: true,
    minPrice: "",
    maxPrice: "",
    sizeL: false,
    sizeM: false,
    sizeS: false,
    empty: false,
  };

  componentDidMount() {
    const currentPath = window.location.pathname;
    const index = currentPath.lastIndexOf("/");
    const categoryId = currentPath.slice(index + 1, currentPath.length);

    axios
      .get(window.backend + "/categories/" + categoryId)
      .then((response) => {
        if (!(response.data["data"] && response.data["data"].length)) {
          console.log("prazno");
          this.setState({ empty: true });
        }

        this.setState({ products: response.data["data"] });
        this.setState({ loading: false });
        if (response.length !== 0) {
          this.setState({
            categoryName: this.state.products[0].category.category_name,
          });
          console.log(this.state.categoryName);
        }
      })
      .catch((err) => console.log(err));
  }

  loadProducts() {
    let productsForDisplay = [];
    let products = [];

    // eslint-disable-next-line
    this.state.products.map((product) => {
      if (
        this.state.minPrice <= product.product_price &&
        this.state.maxPrice === ""
      ) {
        products.push(product);
      } else if (
        this.state.minPrice <= product.product_price &&
        this.state.maxPrice !== "" &&
        this.state.maxPrice >= product.product_price
      ) {
        products.push(product);
      }
    });

    products = this.filterSizes(products);

    products.map((product) =>
      productsForDisplay.push(
        <ProductDisplayCard
          productId={product.id}
          productName={product.product_name}
          productPrice={product.product_price}
          imgURL={product.img_url}
          description={product.desc}
          sizeId={product.sizeId}
          key={product.id}
        />
      )
    );

    return productsForDisplay;
  }

  filterSizes(products) {
    let productsForDisplay = [];
    let i;
    for (i = 0; i < products.length; i++) {
      if (
        this.state.sizeL &&
        products[i].amount.L != null &&
        products[i].amount.L !== 0
      )
        productsForDisplay.push(products[i]);
      else if (
        this.state.sizeM &&
        products[i].amount.M != null &&
        products[i].amount.M !== 0
      )
        productsForDisplay.push(products[i]);
      else if (
        this.state.sizeS &&
        products[i].amount.S != null &&
        products[i].amount.S !== 0
      )
        productsForDisplay.push(products[i]);
      else if (!this.state.sizeL && !this.state.sizeM && !this.state.sizeS)
        productsForDisplay.push(products[i]);
    }

    return productsForDisplay;
  }

  handleClear() {
    this.setState({ sizeL: false });
    this.setState({ sizeM: false });
    this.setState({ sizeS: false });
    this.setState({ minPrice: "" });
    this.setState({ maxPrice: "" });
  }

  render() {
    return (
      <div
        style={{
          backgroundColor: "#e8f4fc",
          border: "1px solid #e8f4fc",
          paddingTop: "1.5%",
        }}
      >
        {this.state.empty === false && (
          <div className="filter">
            <Container fluid>
              <Card style={{ backgroundColor: "#d9e0d9" }}>
                <Row>
                  <Col md={{ span: 2, offset: 1 }}>
                    <Badge
                      pill
                      variant="secondary"
                      style={{
                        marginTop: "0.7rem",
                        display: "flex",
                        justifyContent: "center",
                        fontFamily: "Brush Script MT",
                        color: "#a6b8a6",
                        backgroundColor: "#fcfdff",
                        textShadow:
                          "-0.75px 0 black, 0 0.75px black, 0.75px 0 black, 0 -0.75px black",
                        border: "1.35px dashed #013220",
                      }}
                    >
                      <h1>{this.state.categoryName}</h1>
                    </Badge>
                  </Col>
                  <Col md={{ span: 4, offset: 1 }}>
                    <Form.Group controlId="formBasicText">
                      <Form.Label className="label">Prices:</Form.Label>
                      <Row>
                        <Col>
                          <Form.Control
                            className="textBox"
                            type="text"
                            placeholder="Lowest price"
                            value={this.state.minPrice}
                            onChange={(event) => {
                              if (!isNaN(event.target.value))
                                this.setState({ minPrice: event.target.value });
                            }}
                          />
                        </Col>
                        <Col>
                          <Form.Control
                            className="textBox"
                            type="text"
                            placeholder="Highest price"
                            value={this.state.maxPrice}
                            onChange={(event) => {
                              if (!isNaN(event.target.value))
                                this.setState({ maxPrice: event.target.value });
                            }}
                          />
                        </Col>
                      </Row>
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Label className="label">Sizes:</Form.Label>
                    <ButtonGroup type="checkbox" className="mb-2">
                      <Button
                        variant={this.state.sizeS ? "dark" : "outline-dark"}
                        value={1}
                        onClick={() => {
                          if (!this.state.sizeS) this.setState({ sizeS: true });
                          else this.setState({ sizeS: false });
                        }}
                      >
                        S
                      </Button>
                      <Button
                        variant={this.state.sizeM ? "dark" : "outline-dark"}
                        value={2}
                        onClick={() => {
                          if (!this.state.sizeM) this.setState({ sizeM: true });
                          else this.setState({ sizeM: false });
                        }}
                      >
                        M
                      </Button>
                      <Button
                        variant={this.state.sizeL ? "dark" : "outline-dark"}
                        value={3}
                        onClick={() => {
                          if (this.state.sizeL === false)
                            this.setState({ sizeL: true });
                          else this.setState({ sizeL: false });
                        }}
                      >
                        L
                      </Button>
                    </ButtonGroup>
                  </Col>
                  <Col>
                    <Button
                      variant="dark"
                      style={{ marginTop: "1.6rem" }}
                      onClick={(event) => this.handleClear(event)}
                    >
                      Clear filters
                    </Button>
                  </Col>
                </Row>
              </Card>
            </Container>
            <div className="proizvodi">{this.loadProducts()}</div>
          </div>
        )}
        {this.state.empty && (
          <Alert
            style={{
              textAlign: "center",
              border: "1px solid #e8f4fc",
              backgroundColor: "#e8f4fc",
              fontStyle: "italic",
              fontSize: "1.5rem",
              padding: "5%",
            }}
          >
            <Alert.Heading
              style={{
                fontSize: "2rem",
              }}
            >
              Oh snap! This category has no products!
            </Alert.Heading>
            <p>
              Nothing to see here... Select another{" "}
              <Link
                to="/categories"
                style={{ color: "darkolivegreen", textDecoration: "underline" }}
              >
                category
              </Link>{" "}
              to continue your shopping adventure
            </p>
          </Alert>
        )}
      </div>
    );
  }
}
export default CategoryById;
