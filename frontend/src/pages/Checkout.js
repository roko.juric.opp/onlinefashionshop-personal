import React, { Component } from "react";
import {Button, Form, Col, Row, Image} from "react-bootstrap";
import Axios from "axios";
import "./css/checkout.css";
import PaymentIcon from '@material-ui/icons/Payment';
import picture from "../images/tick.png";
import HomeIcon from '@material-ui/icons/Home';

class Checkout extends Component {
  state = {
    firstName: localStorage.getItem("first_name"),
    lastName: localStorage.getItem("last_name"),
    email: localStorage.getItem("email_confirmed")
      ? localStorage.getItem("email")
      : "",
    adress: "",
    shippingAddress: "",
    phoneNumber: "",
    clicked: false,
    coupon: "",
    couponIsValid: false,
    couponDiscount: 0,
    basket: [],
    underCouponText: "",
  };

  componentDidMount() {
    Axios.get(window.backend + "/baskets/1", {
      headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
    })
      .then((res) => {
        this.setState({ basket: res.data["data"] });
      })
      .catch((err) => console.log(err));
  }

  calculateTotalPrice() {
    let count = 0;
    for (let i = 0; i < this.state.basket.length; i++) {
      count += this.state.basket[i].amount * this.state.basket[i].product_price;
    }
    if (!this.state.couponIsValid) return count;
    else return (count * (100 - this.state.couponDiscount)) / 100;
  }

  async handleSubmit(event) {
    event.preventDefault();
    Axios.post(
      window.backend + "/baskets/checkout",
      {
        firstName: this.state.firstName,
        lastName: this.state.lastName,
        email: this.state.email,
        address: this.state.address,
        shippingAddress: this.state.shippingAddress,
        phoneNumber: this.state.phoneNumber,
        coupon: this.state.coupon,
      },
      { headers: { Authorization: `Bearer ${localStorage.getItem("token")}` } }
    )
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  }

  checkIfCouponIsValid() {
    if (this.state.coupon === "") this.setState({ underCouponText: "" });
    else {
      Axios.get(window.backend + "/coupons/" + this.state.coupon)
        .then((res) => {
          if (res.data["data"][0].is_active) {
            this.setState({
              couponIsValid: true,
              couponDiscount: res.data["data"][0].percentage,
              underCouponText:
                "Coupon has been applied, the discount is " +
                this.state.couponDiscount +
                "%.",
            });
            this.setState({
              couponIsValid: true,
              couponDiscount: res.data["data"][0].percentage,
              underCouponText:
                "Coupon has been applied, the discount is " +
                this.state.couponDiscount +
                "%.",
            });
          } else if (!res.data["data"][0].is_active) {
            this.setState({
              couponIsValid: false,
              couponDiscount: 0,
              underCouponText: "Invalid coupon, no discount has been applied.",
            });
          }
        })
        .catch(() => {
          this.setState({
            couponIsValid: false,
            underCouponText: "Invalid coupon, no discount has been applied.",
          });
        });
    }
  }

  render() {
    if (this.state.clicked === false) {
      return (
        <div className="div-background"  style={{ paddingBottom:"3rem", paddingTop:"6rem", display:"flex", alignItems:"center", justifyContent:"center" }}>
          <Form className="box" style={{width:"30rem"}}>
            <h3 className="login-signin-text" style={{ textAlign: "center" }}>
              <b>Checkout <PaymentIcon/> </b>{" "}
            </h3>
            <Row>
              <Col>
            <Form.Group controlId="formBasicFirstName">
              <Form.Label>First name:</Form.Label>
              <Form.Control
                type="first name"
                placeholder="eg. Mark"
                value={this.state.firstName}
                onChange={(event) =>
                  this.setState({ firstName: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicLastName">
              <Form.Label>Last name:</Form.Label>
              <Form.Control
                type="last name"
                placeholder="eg. Smith"
                value={this.state.lastName}
                onChange={(event) =>
                  this.setState({ lastName: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address:</Form.Label>
              <Form.Control
                type="email"
                placeholder="example@email.com"
                value={this.state.email}
                onChange={(event) =>
                  this.setState({ email: event.target.value })
                }
              />
            </Form.Group>
              </Col>
              <Col>
            <Form.Group controlId="formBasicAddress">
              <Form.Label>Address:</Form.Label>
              <Form.Control
                type="address"
                placeholder="Enter address"
                value={this.state.address}
                onChange={(event) =>
                  this.setState({ address: event.target.value })
                }
              />
            </Form.Group>

            <Form.Group controlId="formBasicShippingAddress">
              <Form.Label>Shipping address:</Form.Label>
              <Form.Control
                type="shipping address"
                placeholder="Enter shipping address"
                value={this.state.shippingAddress}
                onChange={(event) =>
                  this.setState({ shippingAddress: event.target.value })
                }
              />
            </Form.Group>
            <Form.Group controlId="formBasicPhonNumber">
              <Form.Label>Phone number:</Form.Label>
              <Form.Control
                type="phone number"
                placeholder="Enter phone number"
                value={this.state.phoneNumber}
                onChange={(event) =>
                  this.setState({ phoneNumber: event.target.value })
                }
              />
            </Form.Group>
              </Col>
            </Row>
            <div style={{display:"flex", justifyContent:"center"}}>
              <Form.Group controlId="formBasicPhoneNumber">
                <Form.Label>Coupon:</Form.Label>
                <Row>


                    <Form.Control
                        type="coupon"
                        placeholder="Enter your coupon if you have one"
                        value={this.state.coupon}
                        onChange={(event) => {
                          this.setState({ coupon: event.target.value });
                        }}
                        style={{
                          width:"80%"
                        }}

                    />


                    <Button style={{backgroundColor:"#88b8e7", borderColor:"#88b8e7"}} onClick={(event) => this.checkIfCouponIsValid(event)}>
                      +
                    </Button>


                </Row>

                <Form.Text className="text-muted">
                  {this.state.underCouponText}
                </Form.Text>
              </Form.Group>
            </div>

            <Form.Group controlId="ControlTextarea" style={{ textAlign: "center", marginBottom:"0", marginTop:"1.5%" }}>
              <Form.Label>
                <h3 className="login-signin-text">
                  <b >Total price: {this.calculateTotalPrice()} HRK</b>
                </h3>
              </Form.Label>
            </Form.Group>

            <p>
              <br />
              By clicking "Confirm order" you confirm your order and will be
              receiving email with your receipt.
            </p>
            <span>
              <Button
                className="submit-order"
                expand="lg"
                bg="dark"
                variant="dark"
                type="submit"
                onClick={(event) => {
                  this.handleSubmit(event);
                  this.setState({ clicked: true });
                }}
              >
                Confirm order
              </Button>
            </span>

          </Form>
        </div>
      );
    } else {
      return (
        <div style={{
          backgroundColor:"whitesmoke",
          alignContent: "space-around",
          paddingTop: "3rem",
          paddingBottom: "3rem",

        }}
        >
          <div className="confirmation" style={{ textAlign: "center" }}>

              <h3 className="confirmation-text">
                <b>Your order has been confirmed successfully!</b>
              </h3>
              <Image src={picture} alt="tick" height={150} width={150}/>
              <br/>
              <h3 className="thankyou-text">

                <b>Thank you for shopping with us!</b>
              </h3>

            <br />
            You want to continue shopping? Go back to{" "}
            <a href="/" style={{
              color:"#2f4c4f",
              textDecoration:"underline",
            }}
            >
              home page <HomeIcon style={{marginBottom:"0.4%"}}/>
            </a>.
          </div>
        </div>
      );
    }
  }
}
export default Checkout;
