import React, { useState } from "react";
import { Modal, Form, Button, Card } from "react-bootstrap";
import "./css/login.css";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useHistory } from "react-router";

function Login() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  let history = useHistory();

  const handleSubmit = (event) => {
    event.preventDefault();
    axios.defaults.timeout = 10000;

    axios
      .post(
        window.backend + "/login",
        {
          email: email,
          password: password,
        },
        { timeout: 10000 }
      )
      .then((res) => {
        if (res.data.error === null) {
          localStorage.setItem("token", res.data.token);
          const token = localStorage.getItem("token");
          let decoded;
          decoded = jwt_decode(token);
          localStorage.setItem("user_id", decoded.identity.user_id);
          localStorage.setItem("email", decoded.identity.email);
          localStorage.setItem("first_name", decoded.identity.first_name);
          localStorage.setItem("last_name", decoded.identity.last_name);
          localStorage.setItem(
            "email_confirmed",
            decoded.identity.email_confirmed
          );
          localStorage.setItem("role", decoded.identity.role);
          history.push('/');
          history.go(0);
        }
      })
      .catch((err) => {
        console.log(err.code);
        console.log(err.message);
        console.log(err.stack);
        handleShow();
      });
  };
  return (
    <div style={{ marginTop: "3rem", backgroundColor: "#e8f4fc" }}>
      <Card  style={{ backgroundColor: "#e8f4fc", display:"flex", alignItems:"center", justifyContent:"center" }}>
        <Card.Body>
          <Form className="box" style={{ marginTop: "3rem" }}>
            <h3 style={{ textAlign: "center" }}>
              <b>
                Sign In<i className="fa fa-key"/>
              </b>
            </h3>

            <Form.Group controlId="doNotShareInfo">
              <Form.Text className="text-muted">
                Don't worry, we'll never share your login data with anyone else.
              </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label className="label">Email address</Form.Label>
              <Form.Control
                className="textBox"
                type="email"
                placeholder="Enter email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label className="label">Password</Form.Label>
              <Form.Control
                className="textBox"
                type="password"
                placeholder="Enter password"
                value={password}
                onChange={(event) => {
                  setPassword(event.target.value);
                }}
              />
            </Form.Group>

            <span>
              <Button
                className="submit-login-signin"
                expand="lg"
                bg="dark"
                variant="dark"
                type="submit"
                // style={{marginLeft : "40%"}}
                onClick={(event) => handleSubmit(event)}
              >
                Submit
              </Button>
            </span>
            <p>
              <br />
              Don't have an account? Register <a href={"/registration"}>here</a>
              .
            </p>
          </Form>
        </Card.Body>
      </Card>
      {show && (
        <Modal
          show={show}
          onHide={handleClose}
          onShow={() => {
            setEmail("");
            setPassword("");
          }}
          centered
        >
          <Modal.Header closeButton style={{ backgroundColor: "#ffe8eb" }}>
            <Modal.Title>Error</Modal.Title>
          </Modal.Header>
          <Modal.Body style={{ backgroundColor: "#ffe8eb" }}>
            Invalid login! Please try again!
          </Modal.Body>
          <Modal.Footer style={{ backgroundColor: "#ffe8eb" }}>
            <Button
              onClick={handleClose}
              style={{ backgroundColor: "#e06666", borderColor: "#e06666" }}
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    </div>
  );
}
export default Login;
