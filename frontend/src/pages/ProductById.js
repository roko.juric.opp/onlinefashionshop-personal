import React, { Component } from "react";
import axios from "axios";
import hanger from "../images/hanger.png";
import Button from "react-bootstrap/Button";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";

import "./css/ProductById.css";
import { Container, Card, Image, Col, Row } from "react-bootstrap";

class ProductById extends Component {
  state = {
    product: {
      amount: {},
      desc: "",
      id: 0,
      img_url: "",
      product_name: "",
      product_price: 0,
    },
    size: "Select size",
    amount: "Select amount",
  };

  componentDidMount() {
    const currentPath = window.location.pathname;
    const index = currentPath.lastIndexOf("/");
    const productId = currentPath.slice(index + 1, currentPath.length);

    axios
      .get(window.backend + "/products/" + productId)
      .then((response) => {
        this.setState({ product: response.data["data"][0] });
      })
      .catch((err) => console.log(err));
  }

  addToBasket() {
    console.log("usao u add");
    const token = localStorage.getItem("token");
    console.log(this.state.size);
    let size_id = 0;
    if (this.state.size === "S") size_id = 1;
    if (this.state.size === "M") size_id = 2;
    if (this.state.size === "L") size_id = 3;
    axios
      .post(
        window.backend + "/baskets",
        {
          amount: this.state.amount,
          product_id: this.state.product.id,
          size_id: size_id,
        },
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      )
      .then((res) => {
        console.log(res);
        window.history.back();
      })
      .catch((err) => console.log(err));

    localStorage.setItem(
      "basketItemCounter",
      Number(localStorage.getItem("basketItemCounter")) + 1
    );
  }

  handleSelectSize = (e) => {
    this.setState({ size: e });
    if (
      this.state.product.amount[e] == null &&
      this.state.product.amount[e] === 0
    ) {
      this.setState({ amount: "Out of stock" });
    }
    if (this.state.product.amount[e] != null) {
      this.setState({ amount: "Select amount" });
    }
  };

  handleSelectAmount = (e) => {
    this.setState({ amount: e });
  };

  createElements = (n) => {
    let elements = [];
    let i;
    if (n === 0) {
      elements.push(
        <Dropdown.Item eventKey="Out of stock." disabled>
          Out of stock.
        </Dropdown.Item>
      );
    } else {
      for (i = 1; i <= n; i++) {
        elements.push(<Dropdown.Item eventKey={i}>{i}</Dropdown.Item>);
      }
    }
    return elements;
  };

  render() {
    return (
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
          backgroundColor: "#e8f4fc",
        }}
      >
        <div
          style={{
            marginTop: "5%",
            marginBottom: "5%",
          }}
        >
          <Image
            alt="proizvod"
            src={this.state.product.img_url}
            className="product_image"
            height={500}
            width={500}
          />
        </div>

        <Col xs={4} style={{ marginTop: "5%" }}>
          <Row>
            <Image
              src={hanger}
              alt="hanger"
              height={400}
              width={400}
              style={{
                display: "block",
                marginLeft: "auto",
                marginRight: "auto",
              }}
            />
          </Row>
          <Row>
            <Container fluid className="informationBox">
              <Card
                style={{
                  backgroundColor: "whitesmoke",
                  boxShadow: "none",
                  borderColor: "transparent",
                  paddingBottom: "5%",
                }}
              >
                <Card.Title
                  style={{
                    fontSize: "3vw",
                  }}
                >
                  Name: {this.state.product.product_name}
                </Card.Title>
                <Card.Text
                  style={{
                    fontSize: "1.2vw",
                  }}
                >
                  Description: {this.state.product.desc}
                </Card.Text>
                <Card.Subtitle
                  style={{
                    fontSize: "2vw",
                  }}
                >
                  Price: {this.state.product.product_price} HRK
                </Card.Subtitle>
              </Card>

              <div>
                <DropdownButton
                  variant="outline-info"
                  alignRight
                  title={this.state.size}
                  id="dropdown-menu-align-right"
                  onSelect={this.handleSelectSize}
                >
                  <Dropdown.Item eventKey="S">S</Dropdown.Item>
                  <Dropdown.Item eventKey="M">M</Dropdown.Item>
                  <Dropdown.Item eventKey="L">L</Dropdown.Item>
                </DropdownButton>

                <DropdownButton
                  variant="outline-info"
                  hidden={this.state.size === "Select size"}
                  alignRight
                  title={this.state.amount}
                  id="dropdown-menu-align-right"
                  onSelect={this.handleSelectAmount}
                  style={{
                    marginLeft: "6%",
                  }}
                >
                  {this.state.size === "S" &&
                    this.createElements(
                      this.state.product.amount["S"] != null
                        ? this.state.product.amount["S"]
                        : 0
                    )}
                  {this.state.size === "M" &&
                    this.createElements(
                      this.state.product.amount["M"] != null
                        ? this.state.product.amount["M"]
                        : 0
                    )}
                  {this.state.size === "L" &&
                    this.createElements(
                      this.state.product.amount["L"] != null
                        ? this.state.product.amount["L"]
                        : 0
                    )}
                </DropdownButton>
              </div>

              {localStorage.getItem("token") != null &&
                this.state.size !== "Select size" &&
                this.state.amount !== "Select amount" &&
                this.state.amount !== "Out of stock" && (
                  <div>
                    <br />
                    <Button
                      variant="info"
                      onClick={() => this.addToBasket()}
                      style={{ opacity: "0.7" }}
                    >
                      Add to basket
                    </Button>
                  </div>
                )}

              {localStorage.getItem("token") != null &&
                this.state.size !== "Select size" &&
                this.state.amount === "Out of stock" && (
                  <p>
                    <br />
                    Select other size to add to basket!
                  </p>
                )}

              {localStorage.getItem("token") == null && (
                <p>
                  <br />
                  You must <a href="/login">log in</a> to add to basket!
                </p>
              )}
            </Container>
          </Row>
        </Col>
      </div>
    );
  }
}
export default ProductById;
