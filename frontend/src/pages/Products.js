import React, { Component } from "react";
import ProductDisplay from "../components/ProductDisplay";

class Products extends Component {
  render() {
    return (
      <div>
        <ProductDisplay />
      </div>
    );
  }
}

export default Products;
